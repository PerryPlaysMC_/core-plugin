package me.perryplaysmc.plugin.listeners.enchants.bow;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.enchantment.EnchantHandler;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.ingorish.ItemEnchant;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/14/19-2023
 * Package: me.perryplaysmc.plugin.listeners.enchants.bow
 * Path: me.perryplaysmc.plugin.listeners.enchants.bow.AntiGravity
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class AntiGravity implements Listener {
    
    
    @EventHandler
    void onProjectileLaunch(ProjectileLaunchEvent e) {
        if(e.getEntity().getShooter() instanceof Player) {
            if(e.getEntity() instanceof Arrow) {
                User u = CoreAPI.getUser(((Player) e.getEntity().getShooter()).getPlayer());
                ItemEnchant i = new ItemEnchant(u.getItemInHand());
                if(EnchantHandler.getEnchantment("AntiGravity") != null)
                    if(i.hasEnchant(EnchantHandler.getEnchantment("AntiGravity"))) {
                        e.getEntity().setGravity(false);
                    }
            }
        }
    }
}
