package me.perryplaysmc.plugin.listeners.enchants.bow;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.enchantment.EnchantHandler;
import me.perryplaysmc.utils.inventory.ingorish.ItemEnchant;
import me.perryplaysmc.user.User;
import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.List;

public class Teleport implements Listener {


    List<Player> noFall = new ArrayList<>();

    @EventHandler
    void onProjectileHit(ProjectileHitEvent e) {
        if(e.getEntity().getShooter() instanceof Player) {
            if(e.getEntity() instanceof Arrow) {
                if(e.getEntity().hasMetadata("Teleport")) {
                    Player p = (Player) e.getEntity().getShooter();
                    Location x = e.getEntity().getLocation();
                    if(e.getHitEntity() != null) {
                        x = e.getHitEntity().getLocation();
                        float yaw = p.getLocation().getYaw(), pitch = p.getLocation().getPitch();
                        p.teleport(x);
                        p.getLocation().setYaw(yaw);
                        p.getLocation().setPitch(pitch);
                        noFall.add(p);
                        return;
                    }
                    if(x.getBlock().getType().name().contains("AIR")
                    && (x.clone().add(0,1,0).getBlock().getType().name().contains("AIR") ||
                            x.clone().add(0,-1,0).getBlock().getType().name().contains("AIR"))) {
                        Location loc = new Location(x.getWorld(),
                                x.getX(),
                                x.getY(),
                                x.getZ(),
                                p.getLocation().getYaw()
                                , p.getLocation().getPitch());
                        p.teleport(loc);
                        noFall.add(p);
                    }
                }
            }
        }
    }

    @EventHandler
    void onDamage(EntityDamageEvent e) {
        if(!(e.getEntity() instanceof Player)) return;
        if(e.getCause() == EntityDamageEvent.DamageCause.FALL) {
            if(noFall.contains((Player)e.getEntity())) {
                noFall.remove((Player)e.getEntity());
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    void onProjectileLaunch(ProjectileLaunchEvent e) {
        if(e.getEntity().getShooter() instanceof Player) {
            if(e.getEntity() instanceof Arrow) {
                User u = CoreAPI.getUser(((Player) e.getEntity().getShooter()).getPlayer());
                ItemEnchant i = new ItemEnchant(u.getItemInHand());
                if(EnchantHandler.getEnchantment("Teleport") != null)
                    if(i.hasEnchant(EnchantHandler.getEnchantment("Teleport"))) {
                        FixedMetadataValue v = new FixedMetadataValue(Core.getAPI(), true);
                        e.getEntity().setMetadata("Teleport", v);
                    }
            }
        }
    }


}
