package me.perryplaysmc.plugin.listeners.enchants.bow;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.enchantment.EnchantHandler;
import me.perryplaysmc.utils.inventory.ingorish.ItemEnchant;
import me.perryplaysmc.user.User;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class Explosive implements Listener {


    @EventHandler
    void onProjectileHit(ProjectileHitEvent e) {
        if(e.getEntity().getShooter() instanceof Player) {
            if(e.getEntity() instanceof Arrow) {
                if(e.getEntity().hasMetadata("explosive")) {
                    TNTPrimed tnt = e.getEntity().getWorld().spawn(e.getEntity().getLocation(), TNTPrimed.class);
                    tnt.setFuseTicks(-1);
                    tnt.setSilent(false);
                    tnt.setMetadata("fake", new FixedMetadataValue(Core.getAPI(), true));
                    if(e.getEntity().hasMetadata("fireExplode")) {
                        tnt.setMetadata("fireExplode", new FixedMetadataValue(Core.getAPI(), true));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onTntExplode(EntityDamageByEntityEvent e) {
        if(e.getDamager() instanceof TNTPrimed) {
            Entity et = e.getEntity();
            TNTPrimed tnt = (TNTPrimed) e.getDamager();
            if(tnt.hasMetadata("fireExplode")) {
                if(et instanceof LivingEntity) {
                    LivingEntity el = (LivingEntity) et;
                    el.setFireTicks(20*5);
                    if((el.getHealth() +- e.getDamage()) <= 1.5)
                    el.setHealth(1.5);
                }
            }
        }
    }

    @EventHandler
    void onExplode(EntityExplodeEvent e) {
        if(e.getEntity() instanceof TNTPrimed) {
            if(e.getEntity().hasMetadata("fake")){
                e.blockList().clear();
            }
        }
    }

    @EventHandler
    void onProjectileLaunch(ProjectileLaunchEvent e) {
        if(e.getEntity().getShooter() instanceof Player) {
            if(e.getEntity() instanceof Arrow) {
                User u = CoreAPI.getUser(((Player) e.getEntity().getShooter()));
                ItemEnchant i = new ItemEnchant(u.getItemInHand());
                if(EnchantHandler.getEnchantment("Explosive") != null)
                    if(i.hasEnchant(EnchantHandler.getEnchantment("Explosive"))) {
                        FixedMetadataValue v = new FixedMetadataValue(Core.getAPI(), true);
                        e.getEntity().setMetadata("explosive", v);
                        if(u.getItemInHand().getItemMeta().hasEnchant(Enchantment.ARROW_FIRE)) {
                            e.getEntity().setMetadata("fireExplode", v);
                        }
                        e.getEntity().remove();
                    }
            }
        }
    }



}
