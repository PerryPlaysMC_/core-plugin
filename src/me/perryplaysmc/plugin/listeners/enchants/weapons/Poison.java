package me.perryplaysmc.plugin.listeners.enchants.weapons;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.enchantment.CustomEnchant;
import me.perryplaysmc.enchantment.EnchantHandler;
import me.perryplaysmc.utils.inventory.ingorish.ItemEnchant;
import me.perryplaysmc.user.User;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Poison implements Listener {




    @EventHandler
    void onDamage(EntityDamageByEntityEvent e) {
        if(e.getDamager() instanceof Player) {
            User u = CoreAPI.getUser(((Player) e.getDamager()));
            if(u==null || !(e.getEntity() instanceof LivingEntity))return;
            if(u.getItemInHand()!=null&&u.getItemInHand().getType().name().contains("SWORD")) {
                CustomEnchant poison = EnchantHandler.getEnchantment("Poison");
                if(poison != null) {
                    ItemEnchant i = new ItemEnchant(u.getItemInHand());
                    if(i.hasEnchant(poison)) {
                        LivingEntity hit = (LivingEntity)e.getEntity();
                        hit.addPotionEffect(new PotionEffect(PotionEffectType.POISON
                                , calculatePoisonTime(i.getEnchant(poison))*20
                                , calculatePoison(i.getEnchant(poison)), false,true), true);

                    }
                }
            }
        }
    }

    int calculatePoisonTime(int level) {
        if(level == 0) return 0;
        return (level*2)*4;
    }
    int calculatePoison(int level) {
        if(level == 0) return 0;
        return (level);
    }



}
