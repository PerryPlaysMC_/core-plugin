package me.perryplaysmc.plugin.listeners;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import me.perryplaysmc.utils.inventory.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/8/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class MobVoid implements Listener {
    
    ItemStack reset;
    
    public MobVoid() {
        if(!CoreAPI.getSettings().getBoolean("Features.MobVoid.isEnabled"))return;
        ItemBuilder util = ItemBuilder.createItem(new ItemStack(Material.SLIME_BALL, 1));
        util.setName("&5&lMob Void");
        reset = util.buildItem();
        ShapedRecipe r = new ShapedRecipe(reset);
        r.shape("OOO","OCO","OOO");
        r.setIngredient('O', Material.COAL_BLOCK);
        r.setIngredient('C', Material.CHEST);
        Bukkit.addRecipe(r);
    }
    
    List<String> u1 = new ArrayList<>(),u2 = new ArrayList<>();
    
    @EventHandler
    void onMove(PlayerMoveEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        if(u.isCaptured()) {
            e.setTo(e.getFrom());
        }
    }
    
    @EventHandler
    void onEntityClick(PlayerInteractEntityEvent e) {
        if(!CoreAPI.getSettings().getBoolean("Features.MobVoid.isEnabled")&&Bukkit.getPluginManager().getPlugin("Core")!=null)return;
        User u = CoreAPI.getUser(e.getPlayer());
        if(u1.contains(u.getRealName()))return;
        else u1.add(u.getRealName());
        (new BukkitRunnable() {
            @Override
            public void run() {
                u1.remove(u.getRealName());
            }
        }).runTaskLater(Core.getAPI(), 5);
        if(!ItemUtil.isAir(u.getItemInHand())){
            ItemBuilder util =ItemBuilder.createItem(u.getItemInHand());
            if(util.hasDisplayName()&&util.getNoColorDisplayName().equalsIgnoreCase("Mob Void")) {
                if(!u.hasPermission("feature.mobvoid")) {
                    u.sendMessage("[c]You do not have permission for this feature");
                    return;
                }
                if(util.hasKey("EntityType")){
                    u.sendMessage("[c]You already have a mob selected");
                    return;
                }
                Entity e1 = e.getRightClicked();
                if(e1 instanceof Player) {
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        if(p!=(Player)e1) p.hidePlayer(Core.getAPI(), (Player)e1);
                    }
                    Player p = (Player) e1;
                    p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 99999, 10, true, false));
                    User u1 = CoreAPI.getUser(p);
                    u1.setCaptured(true);
                    Location l = u1.getLocation().clone();
                    l.setY(255);
                    l.getBlock().getRelative(BlockFace.DOWN, 2).setType(Material.BARRIER);
                    u1.teleport(l);
                    util.setString("Captured", p.getName());
                    e.getPlayer().getInventory().setItemInMainHand(util.buildItem());
                    u.sendMessage("[c]Selected user [pc]"+ u1.getRealName());
                    return;
                }
                if(e1 instanceof LivingEntity) {
                    LivingEntity i = (LivingEntity) e1;
                    util.setString("EntityType", i.getType().name());
                    if(i.isCustomNameVisible())
                        util.setString("EntityName", i.getCustomName());
                    if(i.hasAI())
                        util.setBoolean("HasAI", true);
                    if(i instanceof Sheep) {
                        Sheep s = (Sheep) i;
                        if(s.isSheared())
                            util.setBoolean("Sheared", true);
                        if(!s.isAdult())
                            util.setBoolean("isBaby", true);
                    }
                    if(i instanceof Cow) {
                        Cow s = (Cow) i;
                        if(!s.isAdult())
                            util.setBoolean("isBaby", true);
                    }
                    if(i instanceof Ageable) {
                        Ageable s = (Ageable) i;
                        if(!s.isAdult())
                            util.setBoolean("isBaby", true);
                    }
                    if(i instanceof Villager){
                        Villager v = (Villager) i;
                        util.setString("Profession", v.getProfession().name());
                        util.setInt("Lvl", v.getVillagerLevel());
                        util.setInt("Exp", v.getVillagerExperience());
                    }
                    util.setDouble("Health", i.getHealth());
                    e.getPlayer().getInventory().setItemInMainHand(util.buildItem());
                    e1.remove();
                    u.sendMessage("[c]Selected mob [pc]"+ e1.getType().name());
                }
            }
        }
    }
    
    @EventHandler
    void onClick(PlayerInteractEvent e) {
        if(!CoreAPI.getSettings().getBoolean("Features.MobVoid.isEnabled"))return;
        User u = CoreAPI.getUser(e.getPlayer());
        if(u1.contains(u.getName()))return;
        if(u2.contains(u.getRealName()))return;
        else u2.add(u.getRealName());
        (new BukkitRunnable() {
            @Override
            public void run() {
                u2.remove(u.getRealName());
            }
        }).runTaskLater(Core.getAPI(), 15);
        if(!ItemUtil.isAir(u.getItemInHand())&&e.getAction().name().contains("BLOCK")) {
            ItemBuilder util = ItemBuilder.createItem(u.getItemInHand());
            if(util.hasDisplayName()&&util.getNoColorDisplayName().equalsIgnoreCase("Mob Void")) {
                if(!u.hasPermission("feature.mobvoid")) {
                    u.sendMessage("[c]You do not have permission for this feature");
                    return;
                }
                if(util.hasKey("Captured")) {
                    String player = util.getString("Captured");
                    User p = CoreAPI.getUser(player);
                    if(p==null) {
                        u.sendMessage("[c]You cannot release this player, they are not online");
                        return;
                    }
                    for(Player p1 : Bukkit.getOnlinePlayers()) {
                        if(p1!=p.getBase()) p1.showPlayer(Core.getAPI(), p.getBase());
                    }
                    p.setCaptured(false);
                    p.getLocation().getBlock().getRelative(BlockFace.DOWN).setType(Material.AIR);
                    p.teleport(e.getClickedBlock().getRelative(e.getBlockFace().getOppositeFace()).getLocation());
                    p.getBase().removePotionEffect(PotionEffectType.BLINDNESS);
    
                    u.sendMessage("[c]Released user [pc]"+util.getString("Captured"));
                    e.getPlayer().getInventory().setItemInMainHand(reset);
                    return;
                }
                if(util.hasKey("EntityType")) {
                    String type = util.getString("EntityType").toUpperCase();
                    LivingEntity et = (LivingEntity) u.getWorld().spawnEntity(e.getClickedBlock().getLocation().add(0.5,1,0.5), EntityType.valueOf(type));
                    if(util.hasKey("EntityName")) {
                        et.setCustomName(util.getString("EntityName"));
                        et.setCustomNameVisible(true);
                    }
                    if(util.hasKey("HasAI"))
                        et.setAI(true);
                    if(et instanceof Sheep) {
                        Sheep s = (Sheep) et;
                        if(util.hasKey("Sheared"))
                            s.setSheared(true);
                        if(util.hasKey("isBaby"))
                            s.setBaby();
                    }
                    if(et instanceof Cow) {
                        Cow s = (Cow) et;
                        if(util.hasKey("isBaby"))
                            s.setBaby();
                    }
                    if(et instanceof Ageable) {
                        Ageable s = (Ageable) et;
                        if(util.hasKey("isBaby"))
                            s.setBaby();
                    }
                    if(et instanceof Villager) {
                        Villager v = (Villager) et;
                        v.setProfession(Villager.Profession.valueOf(util.getString("Profession")));
                        v.setVillagerLevel(util.getInt("Lvl"));
                        v.setVillagerExperience(util.getInt("Exp"));
                    }
                    u.sendMessage("[c]Spawned mob [pc]"+util.getString("EntityType"));
                    e.getPlayer().getInventory().setItemInMainHand(reset);
                }else {
                    u.sendMessage("[c]You must select a mob first!");
                }
            }
        }
        
    }
    
}
