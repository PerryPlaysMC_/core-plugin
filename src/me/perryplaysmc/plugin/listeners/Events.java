package me.perryplaysmc.plugin.listeners;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.User;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/7/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Events implements Listener {
    List<String> sleepers = new ArrayList<>();
    
    @EventHandler
    void sleep(PlayerBedEnterEvent e) {
        if(!CoreAPI.getSettings().getBoolean("Features.OnePlayerSleep.isEnabled"))return;
        if(e.isCancelled()) return;
        sleepers.add(e.getPlayer().getName());
        new BukkitRunnable() {
            public void run() {
                if(!sleepers.contains(e.getPlayer().getName()))return;
                org.bukkit.World w = e.getPlayer().getWorld();
                w.setStorm(false);
                w.setThundering(false);
                e.getPlayer().getWorld().setWeatherDuration(0);
                e.getPlayer().getWorld().setTime(8L);
                CoreAPI.broadcast("[pc]" + e.getPlayer().getName() + "[c] Went to sleep, &o&7Sweet dreams");
                sleepers.remove(e.getPlayer().getName());
            }
            
        }.runTaskLater(Core.getAPI(), 100L);
    }
    
    @EventHandler
    void leaveBed(PlayerBedLeaveEvent e) {
        sleepers.remove(e.getPlayer().getName());
    }
}
