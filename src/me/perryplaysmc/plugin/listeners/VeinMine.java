package me.perryplaysmc.plugin.listeners;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.abstraction.AbstractionHandler;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import me.perryplaysmc.utils.inventory.ingorish.ItemEnchant;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.ingorish.CustomItem;
import me.perryplaysmc.utils.inventory.ItemUtil;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.GameMode;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/5/19-2023
 * <p>
 *
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class VeinMine implements Listener {
    Material axe = ItemBuilder.convertMaterial("GOLD_AXE");

    public boolean editBlock(Block newB, Location start, Location loc, Material type, User u, ItemStack it) {
        boolean x = (u.getBase().getGameMode() == GameMode.CREATIVE) && (u.getBase().getInventory().getItemInMainHand().getType() == axe);
        boolean c = u.getBase().getGameMode() == GameMode.CREATIVE;
        if ((!u.getBase().isSneaking()) || (!c&&((it.getType().getMaxDurability() > 0) && (it.getDurability() >= it.getType().getMaxDurability() + -1)))) return false;
        if ((isOre(newB)) && (newB.getType() != Material.AIR)) {
            if (newB.getType() != type) return false;
            if (it.containsEnchantment(Enchantment.SILK_TOUCH)) {
                loc.getWorld().dropItemNaturally(start, new ItemStack(newB.getType()));
            } else {
                for (org.bukkit.inventory.ItemStack ia : newB.getDrops()) {
                    if (!ia.getType().isBlock()) {
                        ia.setAmount(NumberUtil.getNumDrops(it));
                        ExperienceOrb orb = (ExperienceOrb)loc.getWorld().spawn(loc, ExperienceOrb.class);
                        orb.setExperience(new Random().nextInt(10) + 105);
                    } else { ia.setAmount(1); }
                    loc.getWorld().dropItemNaturally(start, ia);
                }
            }
            if (!c&&it.getType().getMaxDurability() > 0) {
                it = AbstractionHandler.getItemDataUtilHelper().damage(u.getBase(), it, 1);
                u.getItemInHand().setDurability(it.getDurability());
            }
            if (!c&&((it.getType().getMaxDurability() > 0) && (it.getDurability() == it.getType().getMaxDurability() + -1||it.getDurability() == it.getType().getMaxDurability() + -2))) {
                return false;
            }
            u.updateInventory();
            start.getWorld().spawnParticle(Particle.BLOCK_DUST, newB.getLocation(), 20, (
                    Version.isVersionHigherThan(true, Versions.v1_13) ?
                            newB.getBlockData() :
                            new MaterialData(newB.getType(), newB.getData())
            ));
            newB.setType(Material.AIR);
            newB.getState().update(true);
            return true;
        }
        if (newB.getType().name().contains("LEAVES")) {
            if (newB.getType() != type) return false;
            if ((isTool(it)) && (it.containsEnchantment(Enchantment.SILK_TOUCH))) {
                loc.getWorld().dropItemNaturally(start, new ItemStack(newB.getType()));
            } else {
                for (ItemStack ia :  newB.getDrops(it)) {
                    loc.getWorld().dropItemNaturally(start, ia);
                }
            }
            if (!c&&it.getType().getMaxDurability() > 0) {
                it = AbstractionHandler.getItemDataUtilHelper().damage(u.getBase(), it, 1);
                u.getItemInHand().setDurability(it.getDurability());
            }
            if (!c&&((it.getType().getMaxDurability() > 0) && (it.getDurability() == it.getType().getMaxDurability() + -1||it.getDurability() == it.getType().getMaxDurability() + -2))) {
                return false;
            }
            u.updateInventory();
            start.getWorld().spawnParticle(Particle.BLOCK_DUST, newB.getLocation(), 20, (
                    Version.isVersionHigherThan(true, Versions.v1_13) ?
                            newB.getBlockData() :
                            new MaterialData(newB.getType(), newB.getData())
            ));
            newB.setType(Material.AIR);
            newB.getState().update(true);
            return true;
        }
        if ((newB.getType() != Material.AIR) && (isWood(newB.getType()))) {
            for (ItemStack ia : newB.getDrops(it)){
                loc.getWorld().dropItemNaturally(start, ia);
            }
            if ((isWood(newB.getType())) || (isOre(newB))) {
                start.getWorld().spawnParticle(Particle.BLOCK_DUST, newB.getLocation(), 20, (
                        Version.isVersionHigherThan(true, Versions.v1_13) ?
                                newB.getBlockData() :
                                new MaterialData(newB.getType(), newB.getData())
                ));
                newB.setType(Material.AIR);
                newB.getState().update(true);
            }
            return true;
        }
        for (ItemStack ia : newB.getDrops(it)) {
            loc.getWorld().dropItemNaturally(start, ia);
        }
        start.getWorld().spawnParticle(Particle.BLOCK_DUST, newB.getLocation(), 20, (
                Version.isVersionHigherThan(true, Versions.v1_13) ?
                        newB.getBlockData() :
                        new MaterialData(newB.getType(), newB.getData())
        ));
        newB.setType(Material.AIR);
        newB.getState().update(true);
        return true;
    }

    List<User> users = new ArrayList<>();

    @EventHandler
    void onShiftClick(PlayerInteractEvent e) {
        if(!CoreAPI.getSettings().getBoolean("Features.VeinMine.isEnabled"))return;
        User u = CoreAPI.getUser(e.getPlayer());
        if (u.getItemInHand()!=null&&u.getItemInHand().getType() == Material.BOW) return;
        if (((!u.getItemInHand().getType().isBlock()) || (u.getItemInHand().getType() == Material.AIR)) &&
                (e.getAction() == org.bukkit.event.block.Action.RIGHT_CLICK_AIR) && (u.getBase().isSneaking())) {
            if (!users.contains(u))
                users.add(u);
            else return;
            new BukkitRunnable()
            {
                public void run()
                {
                    if (!u.getBase().isSneaking()||!((!u.getItemInHand().getType().isBlock()) ||
                            (u.getItemInHand().getType() == Material.AIR))) {
                        cancel();
                        users.remove(u);
                        return;
                    }
                    users.remove(u);
                    if(!u.hasPermission("feature.veinmine")) {
                        u.sendMessage("[c]You do not have permission for this feature");
                        return;
                    }
                    u.toggleVeinMine();
                    u.sendMessage(new String[] { "[c]VeinMine " + (u.isVeinMine() ? "Enabled" : "Disabled") });
                }
            }.runTaskLater(Core.getAPI(), 5*20);
        }
    }

    @EventHandler
    void onMine(BlockBreakEvent e) {
        if(!CoreAPI.getSettings().getBoolean("Features.VeinMine.isEnabled"))return;
        Block b = e.getBlock();
        Material type = b.getType();
        Player p = e.getPlayer();
        User u = CoreAPI.getUser(p);
        ItemStack it = e.getPlayer().getItemInHand();
        if (e.getPlayer().isSneaking()) {
            if(!u.hasPermission("feature.veinmine") && u.isVeinMine() && u.getBase().isSneaking()) {
                u.sendMessage("[c]You do not have permission for this feature");
                return;
            }
            boolean x = (e.getPlayer().getGameMode() ==GameMode.CREATIVE) && (e.getPlayer().getInventory().getItemInMainHand().getType() == axe);
            boolean c = e.getPlayer().getGameMode() ==GameMode.CREATIVE;
            if ((e.getPlayer().getGameMode() ==GameMode.CREATIVE) && (e.getPlayer().getInventory().getItemInMainHand().getType() != axe)) return;
            Location loc = b.getLocation();
            Material item = e.getPlayer().getItemInHand().getType();
            //ItemStack it = e.getPlayer().getItemInHand();
            int r = 1;
            if (!u.isVeinMine()) return;
            Location l = e.getBlock().getLocation();
            List<Material> allowedMaterials = new ArrayList<>();
            allowedMaterials.addAll(ItemBuilder.getLeaves());
            allowedMaterials.addAll(ItemBuilder.getWoods());


            int max = -1;
            if ((item.name().contains("PICKAXE")) || (item.name().contains("SHOVEL"))) {
                allowedMaterials.clear();
            }
            if ((item.name().contains("SHOVEL")) || (item == Material.AIR)) {
                allowedMaterials.add(Material.GRAVEL);
                if (e.getBlock().getType() == Material.GRAVEL)
                    max = 64;
                allowedMaterials.add(Material.SAND);
                if (e.getBlock().getType() == Material.SAND)
                    max = 64;
            }
            if ((item == ItemBuilder.convertMaterial("WOOD_PICKAXE")) ||
                    (item == ItemBuilder.convertMaterial("GOLD_PICKAXE")) ||
                    (item == Material.STONE_PICKAXE) ||
                    (item == Material.IRON_PICKAXE) ||
                    (item == Material.DIAMOND_PICKAXE) || (x)) {
                allowedMaterials.add(Material.COAL_ORE);
                allowedMaterials.add(Material.LAPIS_ORE);
                allowedMaterials.add(ItemBuilder.convertMaterial("QUARTZ"));
            }
            if ((item == Material.STONE_PICKAXE) || (item == Material.IRON_PICKAXE) || (item == Material.DIAMOND_PICKAXE) || (x)) {
                allowedMaterials.add(Material.IRON_ORE);
            }
            if ((item == Material.IRON_PICKAXE) || (item == Material.DIAMOND_PICKAXE) || (x)) {
                allowedMaterials.add(Material.GOLD_ORE);
                allowedMaterials.add(Material.DIAMOND_ORE);
                allowedMaterials.add(Material.EMERALD_ORE);
                allowedMaterials.add(Material.REDSTONE_ORE);
            }
            if((!ItemUtil.isAir(u.getItemInHand()) &&
                    new ItemEnchant(e.getPlayer().getItemInHand()).hasEnchant(Enchantment.SILK_TOUCH)) || (x)) {
                if(e.getBlock().getType().name().contains("ICE")) {
                    allowedMaterials.add(e.getBlock().getType());
                    max=64;
                }
            }
            allowedMaterials.add(Material.GLOWSTONE);
            if (allowedMaterials.contains(b.getType())) {
                Set<Block> bs = CoreAPI.getBlockHelper().getConnectedBlocks(max, b, allowedMaterials);
                List<Block> blocks = Arrays.asList(bs.toArray(new Block[bs.size()]));
                if ((blocks == null) || (blocks.size() == 0)) {
                    return;
                }
                int interval = CoreAPI.getSettings().getInt("Features.VeinMine.Interval");
                new BukkitRunnable() {
                    int i = 0;
                    int blocksPer = CoreAPI.getSettings().getInt("Features.VeinMine.BlocksPerInterval");

                    public void run() {
                        boolean isItem=!ItemUtil.isAir(u.getItemInHand());
                        if (!u.getBase().isSneaking()||(isItem&&((it.getType().getMaxDurability() > 0) &&
                                (it.getDurability() == (it.getType().getMaxDurability() + -1))))) {
                            cancel();
                            return;
                        }
                        if ((isItem&&(((blocks.size() + -1) < i) || (blocks.get(i) == null) || (i > (blocks.size() + -1))))) {
                            cancel();
                            return;
                        }
                        try {
                            blocks.get(i);
                        } catch (ArrayIndexOutOfBoundsException e) {
                            cancel();
                            return;
                        }
                        if (blocksPer == 1) {
                            editBlock((Block)blocks.get(i), e.getBlock().getLocation(), loc, type, u, it);
                            i += 1;
                            return;
                        }
                        for (int a = 0; a < blocksPer; a++) {
                            if ((!u.getBase().isSneaking()) ||(!c&&(isItem&& (it.getDurability() == it.getType().getMaxDurability() + -2)))) {
                                cancel();
                                return;
                            }
                            Block newB = null;
                            try {
                                newB = (Block)blocks.get(i);
                            } catch (ArrayIndexOutOfBoundsException e) {
                                cancel();
                                return;
                            }
                            editBlock(newB, e.getBlock().getLocation(), loc, type, u, it);
                            i += 1;
                        }
                    }
                }.runTaskTimer(Core.getAPI(), 0L, interval);
            }
        }
    }

    @EventHandler
    void onPlace(BlockPlaceEvent e) {
        if ((e.getItemInHand().hasItemMeta()) &&
                (e.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(StringUtils.translate("[pc]" + StringUtils.getNameFromEnum(e.getItemInHand().getType()) + "[c] Scaffolding")))) {
            Location x = e.getBlockPlaced().getLocation();
            Location a = new Location(x.getWorld(), x.getBlockX(), x.getBlockY(), x.getBlockZ());
            Material type = e.getBlockPlaced().getType();
            org.bukkit.block.data.BlockData bd = e.getBlockPlaced().getBlockData();
            a.getBlock().setType(Material.AIR);
            a.getBlock().getState().update(true, false);
            Location b = distance(a.clone());
            for (int i = 0; i < 255; i++)
                if (b.getBlock().getType().isSolid()) {
                    Core.getAPI().debug(new Object[] { "This block is solid, skipping... ", "X: " + b
                            .getBlockX() + " Y: " + b.getBlockY() + " Z: " + b.getBlockZ() });
                    b = b.add(0.0D, 1.0D, 0.0D);
                }
                else {
                    if (b.getBlockY() == a.getBlockY()) {
                        Core.getAPI().debug(new Object[] { "Ending loop... " + b.getY() + " ae< >a " + a.getY() });
                        b.getBlock().setType(type);
                        b.getBlock().setBlockData(bd);
                        b.getBlock().getState().update(true);
                        Core.getAPI().debug(new Object[] { "Block at: " + b.getY() + " is being changed..." });
                        break;
                    }
                    b.getBlock().setType(type);
                    b.getBlock().setBlockData(bd);
                    b.getBlock().getState().update(true);
                    Core.getAPI().debug(new Object[] { "Block at: " + b.getY() + " is being changed..." });
                    b = b.add(0.0D, 1.0D, 0.0D);
                }
        }
    }

    Location distance(Location top) {
        Location bottom = top;

        for (int i = 0; i < 256; i++) {
            if ((bottom.subtract(0.0D, 1.0D, 0.0D).getBlock().getType().isSolid()) || (bottom.getBlock().getType().isSolid())) {
                break;
            }
            bottom = bottom.subtract(0.0D, 1.0D, 0.0D);
        }

        return bottom;
    }

    boolean isOre(Block b) {
        return isOre(b.getType());
    }

    boolean isWood(Material m) {
        return ItemBuilder.isWood(m);
    }

    boolean isNotSolid(Material m) {
        return (m == Material.COAL_ORE) || (m == Material.DIAMOND_ORE) || (m == Material.LAPIS_ORE) || (m == Material.EMERALD_ORE) ||
                (m == Material.REDSTONE_ORE) || (m == ItemBuilder.convertMaterial("QUARTZ"));
    }

    boolean isOre(Material m)
    {
        return m.name().endsWith("_ORE");
    }

    @EventHandler
    void onSpawnerPlace(BlockPlaceEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        if(!CoreAPI.getSettings().getBoolean("Features.SilkSpawner.isEnabled"))return;
        if (e.getBlockPlaced().getType().name().contains("SPAWNER")) {

            if(!u.hasPermission("feature.silkspawner")) {
                u.sendMessage("[c]You do not have permission for this feature");
                e.setCancelled(true);
                return;
            }
            CustomItem u1 = CustomItem.getData(e.getItemInHand());
            if (!u1.hasKey("type")) return;
            CreatureSpawner s = (CreatureSpawner)e.getBlockPlaced().getState();
            s.setSpawnedType(EntityType.valueOf(u1.getString("type").toUpperCase()));
            s.getBlock().getState().update(true);
            s.update(true);
            if(Version.isVersionHigherThan(true, Versions.v1_13))
                e.getBlockPlaced().setBlockData(s.getBlockData());
            else e.getBlockPlaced().getState().setData(s.getData());
            e.getBlockPlaced().getState().update(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void onSpawnerBreak(BlockBreakEvent e) {
        if(!CoreAPI.getSettings().getBoolean("Features.SilkSpawner.isEnabled"))return;
        User u = CoreAPI.getUser(e.getPlayer());
        Block b = e.getBlock();
        if ((b.getType().name().contains("SPAWNER")) && (u.getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH))) {
            if(!u.hasPermission("feature.silkspawner")) {
                u.sendMessage("[c]You do not have permission for this feature");
                e.setCancelled(true);
                return;
            }
            CreatureSpawner s = (CreatureSpawner)e.getBlock().getState();
            s.getSpawnedType();
            Random r = new Random();
            int chance = r.nextInt(100) + 1;
            if (chance <= CoreAPI.getSettings().getInt("Features.SilkSpawner.Chance")) {
                org.bukkit.inventory.ItemStack i = new ItemStack(b.getType());
                CustomItem u1 = CustomItem.setItem(i);
                String type = StringUtils.getNameFromEnum(s.getSpawnedType());
                u1.setDisplayName("§c" + type + " §cSpawner").setLore("&eInfo:", " §2Type: &b" + type)
                        .setString("type", s.getSpawnedType().name());
                i = u1.finish();
                u.getLocation().getWorld().dropItemNaturally(b.getLocation().add(0.5, 0.5, 0.5), i);
            }
        }
    }

    private boolean isTool(org.bukkit.inventory.ItemStack i) {
        return (i.getType().name().contains("AXE")) || (i.getType().name().contains("SHOVEL")) || (i.getType().name().contains("SWORD"));
    }

}
