package me.perryplaysmc.plugin;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.enchantment.CustomEnchant;
import me.perryplaysmc.enchantment.EnchantTarget;
import me.perryplaysmc.enchantment.Levels;
import me.perryplaysmc.utils.string.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main extends Core {

    static Main inst;


    @Override
    public void onEnable() {
        initialize("Plugin");
        inst = this;
        if (Version.isVersionHigherThan(true, Versions.v1_13)) {
            new CustomEnchant("&aPoison", Levels.LEVELS_25_30, 1, 2, 10, EnchantTarget.WEAPON);
            new CustomEnchant("&6Grapple", Levels.LEVELS_25_30, 1, 1, 7, EnchantTarget.BOW);
            new CustomEnchant("&5Teleport", Levels.LEVELS_25_30, 1, 1, 3, EnchantTarget.BOW);
            new CustomEnchant("&7Explosive", Levels.LEVELS_25_30, 1, 1, 5, EnchantTarget.BOW);
            new CustomEnchant("&7AntiGravity", Levels.LEVELS_25_30, 1, 1, 2.5, EnchantTarget.BOW);
        }

        registerCommands("me.perryplaysmc.plugin.commands");
        registerListeners(this);
        registerListeners("me.perryplaysmc.plugin.listeners");
    }

    @EventHandler
    void onDamage(BlockDamageEvent e) {
        e.setCancelled(true);
        e.setInstaBreak(false);
    }



    public static Main getInstance() {
        return inst;
    }


}
