package me.perryplaysmc.plugin.commands.moderator.prison;

import me.perryplaysmc.chat.Info;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.event.user.UserInteractEvent;
import me.perryplaysmc.event.user.block.Action;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.exceptions.InvalidLocationException;
import me.perryplaysmc.prison.ah.AuctionHouse;
import me.perryplaysmc.prison.cells.ExpireDate;
import me.perryplaysmc.prison.cells.PrisonCell;
import me.perryplaysmc.prison.cells.PrisonCellManager;
import me.perryplaysmc.prison.cells.Region;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.GuiUtil;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import me.perryplaysmc.utils.inventory.menus.ListGui;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.ListsHelper;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.world.BlockHelper;
import me.perryplaysmc.world.LocationUtil;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2023
 * Package: me.perryplaysmc.plugin.commands.moderator.prison
 * Class: CommandCells
 * <p>
 * Path: me.perryplaysmc.plugin.commands.moderator.prison.CommandCells
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandCells extends Command implements Listener {

    private HashMap<User, Location> pos1 = new HashMap<>();
    private HashMap<User, Location> pos2 = new HashMap<>();
    private HashMap<User, Location> ahPos1 = new HashMap<>();
    private HashMap<User, Location> ahPos2 = new HashMap<>();
    private HashMap<User, PrisonCell> cells = new HashMap<>();
    private HashMap<User, AuctionHouse> ahs = new HashMap<>();
    private HashMap<User, BukkitTask> tasks = new HashMap<>();
    private GuiUtil gui;
    ItemStack wand, sign, wand2;

    public CommandCells() {
        super("cells", "cell");
        setDescription("The main Cells command");
        wand = ItemBuilder
                .createItem(Material.BLAZE_ROD)
                .setName("&c&lCell Wand")
                .addLore(
                        "[c]Set the region of a cell",
                        "[c]Left click to set [pc]Pos1",
                        "[c]Right click to set [pc]Pos2"
                )
                .buildItem();
        sign = ItemBuilder
                .createItem("SIGN")
                .setName("&c&lCell Sign")
                .addLore(
                        "[c]Step 1: Select a cell",
                        "[c]Step 2: Place this sign"
                )
                .buildItem();
        wand2 = ItemBuilder
                .createItem(Material.SLIME_BALL)
                .setName("&c&lAuctionHouse wand")
                .addLore(
                        "[c]Set the region of a auctionhouse",
                        "[c]Left click to set [pc]Pos1",
                        "[c]Right click to set [pc]Pos2"
                )
                .buildItem();
        addDefaultPermission("cells", "cells.*");
    }

    void setupGui(User user, GuiUtil gui) {
        int slot = 0;
        int index = 0;
        for(String g : PrisonCellManager.getGroups()) {
            gui.setItem(index, ItemBuilder.createItem("SIGN").setName("&cGroup: &e"+g.toUpperCase()));
            index++;
        }
        gui.onClick((e) -> {
            ItemStack current = e.getCurrentItem();
            e.setCancelled(true);
            if(current==null)return;
            if(!current.hasItemMeta())return;
            ItemMeta im = current.getItemMeta();
            String group = ColorUtil.removeColor(im.getDisplayName().split("§cGroup: §e")[1].toUpperCase());
            int size = PrisonCellManager.getCells(group).size();
            int islot = 0;
            ListGui ng = ListGui.createGui(gui, "&c&lCells: &e" + group);
            for(PrisonCell cell : PrisonCellManager.getCells(group)) {
                ng.addItem(ItemBuilder.createItem("IRON_BARS").setName("§cCell: §e" + cell.getId())
                        .setLore(
                                "[c]Min: " + getLocation(cell.getRegion().getMin()),
                                "[c]Max: " + getLocation(cell.getRegion().getMax()),
                                (cell.isRented() ?
                                "[c]RenewPrice: [pc]" + cell.getRenewPrice()
                                        : "[c]Cost: " + cell.getBuyCost())
                        )
                        .buildItem());
            }
            ng.onClick((e1)-> {
                ItemStack cur = e1.getCurrentItem();
                e1.setCancelled(true);
                if(cur==null)return;
                if(!cur.hasItemMeta())return;
                ItemMeta i = cur.getItemMeta();
                int id = (int) NumberUtil.findNumber(i.getDisplayName());
                if(id > -1) {
                    PrisonCell cell = PrisonCellManager.getCell(group, id);
                    if(cell != null) {
                        if(cell.getSign()!=null) {
                            user.teleport(cell.getSign().getLocation().clone()
                                    .subtract(0,1,0).add(0.5, 0, 0.5)
                                    .setDirection(
                                            user.getLocation().getDirection()
                                                    .setX(cell.getSignFace().getModX())
                                                    .setY(cell.getSignFace().getModY())
                                                    .setZ(cell.getSignFace().getModZ())
                                    ));
                        }
                        user.getBase().closeInventory();
                    }
                }
            });
            ng.send(user, 1);
        });
        if(this.gui!=null&&gui.size()!=this.gui.size())
            this.gui.remove();
        this.gui = gui;
        user.openInventory(gui.getResult());
    }

    @Override
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        mustBe(true);
        User u = s.getUser();
        if(args.length == 0) {
            hasPermission("cells");
            sendHelp();
            return;
        }
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("wand")) {
                hasPermission("cells.wand");
                if(u.getInventory().contains(wand))
                    sendMessageEnd("Cells.Wand.fail");
                u.getInventory().addItem(wand);
                sendMessage("Cells.Wand.success");
                return;
            }
            if(args[0].equalsIgnoreCase("sign")) {
                hasPermission("cells.sign");
                if(u.getInventory().contains(sign))
                    sendMessageEnd("Cells.Sign.fail");
                u.getInventory().addItem(sign);
                sendMessage("Cells.Sign.success");
                return;
            }
            if(args[0].equalsIgnoreCase("reloadsigns")) {
                hasPermission("cells.reload.sign");
                PrisonCellManager.refreshSigns();
                sendMessage("Cells.Reload.sign");
                return;
            }
            if(args[0].equalsIgnoreCase("reloadcells")) {
                hasPermission("cells.reload.cells");
                PrisonCellManager.loadCells();
                sendMessage("Cells.Reload.cell");
                return;
            }
            if(args[0].equalsIgnoreCase("list")) {
                int size = PrisonCellManager.getGroups().size() <= 5 ? 5
                        : PrisonCellManager.getGroups().size() <= 9 ? 9
                        : PrisonCellManager.getGroups().size() <= 18 ? 18
                        : PrisonCellManager.getGroups().size() <= 27 ? 27
                        : PrisonCellManager.getGroups().size() <= 36 ? 36
                        : PrisonCellManager.getGroups().size() <= 45 ? 45
                        : 54;
                setupGui(u, gui == null || gui.size()!=size ? new GuiUtil("                &c&lCells", size): gui);
                return;
            }
            if(args[0].equalsIgnoreCase("show")) {
                Player p = s.getUser().getBase();
                if(!tasks.containsKey(u)) {

                    for(PrisonCell cell : PrisonCellManager.getCells()) {
                        cell.getRegion().clear();
                    }
                    BukkitTask t = (new BukkitRunnable() {
                        @Override
                        public void run() {
                            List<PrisonCell> cells = PrisonCellManager.getClosestToPlayer(p, 15);
                            if(cells.size() == 0) {
                                cancel();
                                if(tasks.containsKey(u))
                                    tasks.remove(u);
                                for(PrisonCell cell : PrisonCellManager.getCells()) {
                                    cell.getRegion().clear();
                                }
                                return;
                            }
                            for(PrisonCell cell : cells) {
                                if(!p.isOnline()) {
                                    cancel();
                                    if(tasks.containsKey(u))
                                        tasks.remove(u);
                                    return;
                                }
                                for(PrisonCell cell1 : PrisonCellManager.getCells()) {
                                    if(!cells.contains(cell1))
                                        cell1.getRegion().clear();
                                }
                                cell.getRegion().outLine(false, p);
                            }
                        }
                    }).runTaskTimer(Core.getAPI(), 0, 10);
                    tasks.put(u, t);
                }else {
                    tasks.get(u).cancel();
                    tasks.remove(u);
                    for(PrisonCell cell : PrisonCellManager.getCells()) {
                        cell.getRegion().clear();
                    }
                }
                return;
            }
            sendHelp();
            return;
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("ah", "auctionhouse")) {
                if(args[1].equalsIgnoreCase("wand")) {
                    hasPermission("auctionhouse.wand");
                    if(u.getInventory().contains(wand2))
                        sendMessageEnd("Cells.AuctionHouse.Wand.fail");
                    u.getInventory().addItem(wand2);
                    sendMessage("Cells.AuctionHouse.Wand.success");
                    return;
                }
            }
            sendHelp();
            return;
        }
        if(args.length == 3) {
            if(args[0].equalsIgnoreCase("evict")) {
                if(!PrisonCellManager.getGroups().contains(args[2].getContents().toLowerCase()))
                    sendMessageEnd("Cells.invalidGroup", args[2].getContents().toLowerCase());
                OfflineUser t = getAnyUser(args[1]);
                PrisonCell cell = PrisonCellManager.getCell(args[2].getContents(), t.getUniqueId());
                if(cell == null)
                    sendMessageEnd("Cells.Evict.fail", t.getRealName(), args[2].getContents().toLowerCase());
                cell.unrent();
                sendMessage("Cells.Evict.success", t.getRealName(), args[2].getContents().toLowerCase());
                return;
            }
            if(args[0].equalsIgnoreCase("ah", "auctionhouse")) {
                if(args[1].equalsIgnoreCase("create")) {
                    if(!PrisonCellManager.getGroups().contains(args[2].getContents()))
                        sendMessageEnd("Cells.invalidCellBlock");
                    ahs.put(u, new AuctionHouse(args[2].getContents()));
                    sendMessage("Cells.AuctionHouse.create");
                    return;
                }
            }
            if(args[0].equalsIgnoreCase("ah", "auctionhouse")) {
                if(args[1].equalsIgnoreCase("select")) {
                    if(!PrisonCellManager.getGroups().contains(args[2].getContents().toLowerCase()))
                        sendMessageEnd("Cells.invalidCellBlock");
                    sendMessage("Cells.AuctionHouse.select", args[2].toLowerCase());
                    return;
                }
            }
            if(args[0].equalsIgnoreCase("select")) {
                hasPermission("cells.select");
                PrisonCell cell = PrisonCellManager.getCell(args[1].getContents(), integer(args[2]));
                if(cell == null)
                    sendMessageEnd("Cells.Select.fail", args[1].getContents(), integer(args[2]));
                sendMessage("Cells.Select.success", args[1].getContents(), integer(args[2]));
                cells.put(u, cell);
                return;
            }
            sendHelp();
            return;
        }
        if(args.length == 4) {
            if(args[0].equalsIgnoreCase("create")) {
                hasPermission("cells.create");
                String group = args[1].getContents();
                if(!pos1.containsKey(u))
                    sendMessageEnd("Cells.Pos.not-set-pos1");
                if(!pos2.containsKey(u))
                    sendMessageEnd("Cells.Pos.not-set-pos2");
                Location pos1 = this.pos1.get(u);
                Location pos2 = this.pos2.get(u);
                double price = money(args[3]);
                loopCells(u, group, new ExpireDate(getTime(args[2].getContents())), price);
                sendMessage("Cells.Create.success",
                        group.toLowerCase(),
                        getLocation(pos1),
                        getLocation(pos1),
                        ""+
                                getTime(args[2].getContents())[0] + "d," +
                                getTime(args[2].getContents())[1] + "h," +
                                getTime(args[2].getContents())[2] + "m",
                        price
                );
                return;
            }
            sendHelp();
            return;
        }
        sendHelp();
    }

    int[] getTime(String toParse) throws CommandException{
        int[] r = new int[3];
        String current = "";
        int index = 0;
        for(char c : toParse.toCharArray()) {
            if(c!=',') {
                current+=c;
            }else {
                r[index] = integer(new Argument(current));
                current = "";
                index++;
            }
        }
        r[index] = integer(new Argument(current));
        return r;
    }

    boolean isAir(Material m) {
        return m.name().endsWith("AIR") && !m.name().endsWith("AIRS");
    }

    void addSection(AuctionHouse ah, Location pos1, Location pos2) {
        Region region = new Region(pos1, pos2);
        if(pos1.getBlockZ() == pos2.getBlockZ() || pos1.getBlockX() == pos2.getBlockX()) {
            ah.addRegion(region);
            return;
        }
        sendMessageRaw("Bad");
    }

    void loopCells(User u, String group, ExpireDate date, double price) {
        Location min1 = this.pos1.get(u);
        Location max1 = this.pos2.get(u);
        if(max1==null||min1==null) {
            try {
                throw new InvalidLocationException("Please set both locations.");
            } catch (InvalidLocationException e) {
                e.printStackTrace();
            }
        }
        if(max1.getWorld().getName()!=min1.getWorld().getName()) {
            try {
                throw new InvalidLocationException("Cannot create points between two different worlds.");
            } catch (InvalidLocationException e) {
                e.printStackTrace();
            }
            return;
        }
        Region rg = new Region(min1, max1);
        Location max = rg.getMax();
        Location min = rg.getMin();
        final int[] delay = {0};
        int rX = max.getBlockX()+1;
        int rY = max.getBlockY()+1;
        int rZ = max.getBlockZ()+1;
        List<Region> regions = new ArrayList<>();
        HashMap<Location, BlockState> previous = new HashMap<>();
        for(int y = min.getBlockY(); y < rY; y++) {
            delay[0]+=5;
            for(int x = min.getBlockX(); x < rX; x++) {
                for(int z = min.getBlockZ(); z < rZ; z++) {
                    Block b = u.getWorld().getBlockAt(x,y,z);
                    if(!(b.getType().name().endsWith("AIR") && !b.getType().name().endsWith("AIRS"))) {
                        previous.put(b.getLocation(), b.getState());
                        if(b.getType().isSolid())
                            setBlock(b, Material.BARRIER, delay[0]);
                        else
                            setBlock(b, Material.AIR, delay[0]);
                    }
                }
            }
        }
        HashMap<Region, List<Block>> blocksH = new HashMap<>();
        HashMap<Region, List<Block>> blocks1H = new HashMap<>();
        (new BukkitRunnable() {
            public void run() {
                for(int y = min.getBlockY(); y < rY; y++) {
                    delay[0] += 10;
                    for(int x = min.getBlockX(); x < rX; x++) {
                        for(int z = min.getBlockZ(); z < rZ; z++) {
                            Block b = u.getWorld().getBlockAt(x, y, z);
                            if(b.getType().name().endsWith("AIR") && !b.getType().name().endsWith("AIRS")) {
                                BlockHelper bh = CoreAPI.getBlockHelper();
                                Set<Block> blocks = bh.getConnectedBlocks(-1, new Region(min, max), b);
                                int minX = Integer.MAX_VALUE, maxX = Integer.MIN_VALUE;
                                int minY = Integer.MAX_VALUE, maxY = Integer.MIN_VALUE;
                                int minZ = Integer.MAX_VALUE, maxZ = Integer.MIN_VALUE;
                                List<Block> blocks1 = new ArrayList<>();
                                List<Block> blocks2 = new ArrayList<>();
                                C:for(Block block : blocks) {
                                    Location b1 = block.getLocation();
                                    int bx = b1.getBlockX();
                                    int by = b1.getBlockY();
                                    int bz = b1.getBlockZ();
                                    int i = 0;

                                    if(block.getType() == Material.AIR) {
                                        if(previous.containsKey(block.getLocation()))
                                            blocks2.add(block);
                                        for(BlockFace f : new BlockFace[] {
                                                BlockFace.NORTH,
                                                BlockFace.SOUTH,
                                                BlockFace.WEST,
                                                BlockFace.EAST,
                                                BlockFace.DOWN,
                                                BlockFace.UP
                                        }) {
                                            Block block1 = block.getRelative(f);
                                            if(block1.getType() == Material.BARRIER)
                                                i++;
                                        }
                                        if(i < 3){
                                            blocks1.add(block);
                                        }
                                    }
                                    if(i<3) {
                                        if(bx > maxX)
                                            maxX = bx;
                                        if(bx < minX)
                                            minX = bx;

                                        if(by > maxY)
                                            maxY = by;
                                        if(by < minY)
                                            minY = by;

                                        if(bz > maxZ)
                                            maxZ = bz;
                                        if(bz < minZ)
                                            minZ = bz;
                                    }
                                }
                                if(!((minZ == Integer.MAX_VALUE || minX == Integer.MAX_VALUE) ||
                                        (maxX == Integer.MIN_VALUE || maxZ == Integer.MAX_VALUE))) {
                                    Location min1 = new Location(min.getWorld(), minX, minY, minZ),
                                            max1 = new Location(min.getWorld(), maxX, maxY, maxZ);
                                    Region r1 = new Region(min1, max1);
                                    if(!regionExists(regions, r1)) {
                                        regions.add(r1);
                                        blocksH.put(r1, blocks1);
                                        blocks1H.put(r1, blocks2);
                                    }
                                    if(x < max1.getBlockX())
                                        x = max1.getBlockX();
                                    if(z < max1.getBlockZ())
                                        z = max1.getBlockZ();
                                }
                            }
                        }
                    }
                }
            }
        }).runTaskLater(Core.getAPI(), delay[0]+=10);
        int x = delay[0]+=10;
        delay[0]=0;
        (new BukkitRunnable() {
            @Override
            public void run() {
                /*for(Map.Entry<Location, BlockState> map : previous.entrySet()) {
                    setBlock(map.getKey().getBlock(), map.getValue(), 10);
                }*/
                if(true) {
                    Region r = null;
                    try {
                        r = findMinMax(previous.keySet());
                    } catch (Exception e) {

                        s.sendMessage(e.getMessage());
                        return;
                    }
                    Location min = r.getMin();
                    Location max = r.getMax();
                    int rX = max.getBlockX()+1;
                    int rY = max.getBlockY()+1;
                    int rZ = max.getBlockZ()+1;
                    for (int y = min.getBlockY(); y < rY; y++) {
                        delay[0] += 4;
                        for (int x = min.getBlockX(); x < rX; x++) {
                            for (int z = min.getBlockZ(); z < rZ; z++) {
                                Block b = u.getWorld().getBlockAt(x, y, z);
                                if (previous.containsKey(b.getLocation())) {
                                    BlockState state = previous.get(b.getLocation());
                                    setBlock(b, state, delay[0]);
                                }
                                for(Region r1 : regions) {
                                    if(r1.isWithinBoarder(b.getLocation()) && blocksH.containsKey(r1) && blocksH.get(r1).contains(b)) {
                                        Material x1 = ItemBuilder.convertMaterial("stained_glass");
                                        DyeColor[] dc = new DyeColor[]{
                                                DyeColor.MAGENTA,
                                                DyeColor.PURPLE,
                                                DyeColor.ORANGE,
                                                DyeColor.YELLOW
                                        };

                                        BlockState state = b.getState();
                                        state.setType(x1);
                                        state.setRawData(dc[new Random().nextInt(dc.length)].getDyeData());
                                        setBlock(b, state, 10);
                                    }
                                }
                            }
                        }
                    }
                }
                (new BukkitRunnable() {

                    public void run() {
                        delay[0] = 0;
                        for(Region r : regions) {
                            if(!PrisonCellManager.cellExists(r)) {
                                AtomicReference<Sign> s = new AtomicReference<>();
                                r.loop((b) -> {
                                    for(BlockFace f : BlockFace.values()) {
                                        if(f == BlockFace.UP || f == BlockFace.DOWN) continue;
                                        if(b.getRelative(f).getState() instanceof Sign && s.get() == null) {
                                            if(PrisonCellManager.getCell((Sign) b.getRelative(f).getState()) == null
                                                    && rg.isWithinBoarder(b.getRelative(f).getLocation())) {
                                                s.set((Sign) b.getRelative(f).getState());
                                            }
                                        }
                                    }
                                });
                                PrisonCell p = new PrisonCell(group, r, date, price);
                                if(s.get() != null) {
                                    p.setSign(s.get());
                                    p.recalculatePrice(true);
                                }
                                if(blocks1H.get(r) != null)
                                    for(Block b : blocks1H.get(r)) {
                                        if(!blocksH.containsKey(r)|| !blocksH.get(r).contains(b))
                                            p.addDefaultBlock(b);
                                    }
                            }
                            if(blocksH.get(r) != null) {
                                Location min = r.getMin();
                                Location max = r.getMax();
                                int rX = max.getBlockX()+1;
                                int rY = max.getBlockY()+1;
                                int rZ = max.getBlockZ()+1;
                                for (int y = min.getBlockY(); y < rY; y++) {
                                    delay[0] += 3;
                                    for (int x = min.getBlockX(); x < rX; x++) {
                                        for (int z = min.getBlockZ(); z < rZ; z++) {
                                            Block b = u.getWorld().getBlockAt(x, y, z);
                                            if(blocksH.get(r).contains(b))
                                                if(!blocks1H.containsKey(r) || !blocks1H.get(r).contains(b)) {
                                                    setBlock(b, Material.AIR, delay[0]);
                                                }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }).runTaskLater(Core.getAPI(), delay[0]+10);
            }
        }).runTaskLater(Core.getAPI(), x);
    }

    public Region findMinMax(Iterable<Location> locs) throws Exception {
        int minX = Integer.MAX_VALUE, maxX = Integer.MIN_VALUE;
        int minY = Integer.MAX_VALUE, maxY = Integer.MIN_VALUE;
        int minZ = Integer.MAX_VALUE, maxZ = Integer.MIN_VALUE;
        World w = null;
        C:for(Location b2 : locs) {
            Block b1 = b2.getBlock();
            int bx = b1.getX();
            int by = b1.getY();
            int bz = b1.getZ();
            if(w == null)
                w = b1.getWorld();
            if(!b1.getWorld().getName().equalsIgnoreCase(w.getName())) {
                throw new InvalidLocationException("Invalid locations");
            }
            if(bx > maxX)
                maxX = bx;
            if(bx < minX)
                minX = bx;

            if(by > maxY)
                maxY = by;
            if(by < minY)
                minY = by;

            if(bz > maxZ)
                maxZ = bz;
            if(bz < minZ)
                minZ = bz;
        }
        Location min1 = new Location(w, minX, minY, minZ),
                max1 = new Location(w, maxX, maxY, maxZ);
        Region r1 = new Region(min1, max1);
        return r1;
    }

    boolean regionExists(List<Region> regions, Region r) {
        for(Region rg : regions) {
            if(LocationUtil.isSimilar(rg.getMax(), r.getMax())&&LocationUtil.isSimilar(rg.getMin(), r.getMin()))
                return true;
        }
        return false;
    }

    boolean isWithin(Location max, Location min, Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        return ((x >= xmin && x <= xmax) && (y>=ymin&&y<=ymax) && (z >= zmin && z <= zmax));
    }

    void setBlock(Block b, BlockState state, int delay) {
        (new BukkitRunnable(){
            @Override
            public void run() {
                BlockState bs = b.getState();
                BlockState s = state;
                bs.setType(s.getType());
                if(Version.isVersionHigherThan(true, Versions.v1_13))
                    bs.setBlockData(s.getBlockData());
                else {
                    bs.setData(s.getData());
                    bs.setRawData(s.getRawData());
                }
                bs.update(true);
            }
        }).runTaskLater(Core.getAPI(), delay);
    }
    void setBlock(Block b, Material material, int delay) {
        (new BukkitRunnable(){
            @Override
            public void run() {
                b.setType(material);
                b.getState().update(true);
            }
        }).runTaskLater(Core.getAPI(), delay);
    }

    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException, IllegalArgumentException {
        List<String> f = new ArrayList<>();
        f.addAll(loadTab(1, ListsHelper.createList("evict", "auctionhouse","wand",
                "sign", "create", "select", "reloadcells", "reloadsigns", "show")));
        f.addAll(loadTab(2, "create", ListsHelper.createList("A")));
        f.addAll(loadTab(2, new ArgumentData(0, "select", "auctionhouse"), PrisonCellManager.getGroups()));
        if(args.length == 3 && PrisonCellManager.getCells(args[1].getContents())!=null)
            f.addAll(loadTab(3, new ArgumentData(0, "select"), PrisonCellManager.getCellIds(args[1].getContents())));
        if(args.length == 3 && CoreAPI.getAllUser(args[1].getContents())!=null)
            f.addAll(loadTab(3, new ArgumentData(0, "evict"), PrisonCellManager.getCells(
                    CoreAPI.getAllUser(args[1].getContents()))));
        f.addAll(loadTab(2, new ArgumentData(0, "evict"), CoreAPI.getAllUserNames()));
        f.addAll(loadTab(3, new ArgumentData(0, "create"), ListsHelper.createList("3")));
        f.addAll(loadTab(4, new ArgumentData(0, "create"), ListsHelper.createList("6000", "12000")));
        return f;
    }

    @EventHandler
    void onPlace(BlockPlaceEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        if(e.getItemInHand().isSimilar(sign) && !cells.containsKey(u)) {
            e.setCancelled(true);
            sendMessage(u, "Cells.mustSelect");
            return;
        }
        if(e.getItemInHand().isSimilar(sign) && cells.containsKey(u)) {
            Block b = e.getBlockPlaced();
            Sign s = (Sign) b.getState();
            s.setEditable(false);
            s.update(true);
            cells.get(u).setSign(s);
            cells.get(u).recalculatePrice(true);
        }
    }

    @EventHandler
    void onClick(UserInteractEvent e) {
        if(e.getActionType() == Action.INTERACT) {
            if(e.getUser().getItemInHand().isSimilar(wand)) {
                if(e.getAction() == Action.LEFT_CLICK_BLOCK) {
                    pos1.put(e.getUser(), e.getBlock().getLocation());
                    sendMessage(e.getUser(),"Cells.Pos.set-pos1", getLocation(e.getBlock().getLocation()));
                }
                if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    pos2.put(e.getUser(), e.getBlock().getLocation());
                    sendMessage(e.getUser(),"Cells.Pos.set-pos2", getLocation(e.getBlock().getLocation()));
                }
                e.setCancelled(true);
            }
            if(e.getUser().getItemInHand().isSimilar(wand2)) {
                if(e.getAction() == Action.LEFT_CLICK_BLOCK) {
                    ahPos1.put(e.getUser(), e.getBlock().getLocation());
                    sendMessage(e.getUser(),"Cells.Pos.set-pos1", getLocation(e.getBlock().getLocation()));
                }
                if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    ahPos2.put(e.getUser(), e.getBlock().getLocation());
                    sendMessage(e.getUser(),"Cells.Pos.set-pos2", getLocation(e.getBlock().getLocation()));
                }
                e.setCancelled(true);
            }
        }
    }

    void sendHelp() {
        Info info = new Info(this);
        info.addSub("wand")
                .tooltip(
                        "[c]Gives you the wand",
                        "[c]for setting the regions of a cell"
                )
                .command("/cells wand");
        info.addSub("sign")
                .tooltip(
                        "[c]Gives you a sign to place ",
                        "[c]with the lines for a cell all written"
                )
                .command("/cells sign");
        info.addSub("reloadcells")
                .tooltip(
                        "[c]Reloads all cells and the config"
                )
                .command("/cells reloadcells");
        info.addSub("reloadsigns")
                .tooltip(
                        "[c]Reloads all cell signs"
                )
                .command("/cells reloadsigns");
        info.addSub("show")
                .tooltip(
                        "[c]Outlines all cell regions with particles"
                )
                .command("/cells show");
        info.addSub("list")
                .tooltip(
                        "[c]List all cells in a gui"
                )
                .command("/cells list");
        info.addSub("select")
                .tooltip(
                        "[c]Select a cell to edit"
                )
                .suggest("/cells select ");
        info.addSub("evict <player> <group>")
                .tooltip(
                        "[c]Evict a player from a specified cell block"
                )
                .suggest("/cells evict ");
        info.addSub("create <group> <days,hours,minutes> <price>")
                .tooltip("[c]Creates a Cell",
                        "[c]Arguments:",
                        " [c]- [r]<group>[c] Set cell group/ward",
                        " [c]- [r]<days,hours,minutes>[c] How long the cell will last before it expires",
                        " [c]- [r]<money>[c] How much the cell costs",
                        "[c]Example: /[pc]cells create A 0,5,0(5 hours) 6000"
                )
                .suggest("/cells create ");

        info.send(s);
    }

    String getLocation(Location l) {
        return "[c]X: [pc]" + l.getBlockX() + " [c]Y: [pc]" + l.getBlockY() + " [c]Z: [pc]" + l.getBlockZ();
    }


}
