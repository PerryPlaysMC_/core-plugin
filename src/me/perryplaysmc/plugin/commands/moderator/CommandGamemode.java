package me.perryplaysmc.plugin.commands.moderator;

import com.google.common.collect.Lists;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.ListsHelper;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.GameMode;

import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/18/19-2023
 * Package: me.perryplaysmc.plugin.commands.moderator
 * Class: CommandGamemode
 * <p>
 * Path: me.perryplaysmc.plugin.commands.moderator.CommandGamemode
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandGamemode extends Command {
    
    public CommandGamemode() {
        super("gamemode", "gm", "gmc", "gms", "gma", "gmsp", "gamemodec", "gamemodes", "gamemodea", "gamemodesp");
        setDescription("Sets the users GameMode");
    }
    
    public void run(CommandSource s, CommandLabel cl, Argument[] args) throws CommandException {
        if(cl.equalsIgnoreCase("gmc", "gamemodec")) {
            if(args.length == 0) {
                specifyUser();
                setGameMode(s.getUser(), GameMode.CREATIVE);
                return;
            }
            setGameMode(getUser(args[0]), GameMode.CREATIVE);
            return;
        }
        if(cl.equalsIgnoreCase("gms", "gamemodes")) {
            if(args.length == 0) {
                specifyUser();
                setGameMode(s.getUser(), GameMode.SURVIVAL);
                return;
            }
            setGameMode(getUser(args[0]), GameMode.SURVIVAL);
            return;
        }
        if(cl.equalsIgnoreCase("gma", "gamemodea")) {
            if(args.length == 0) {
                specifyUser();
                setGameMode(s.getUser(), GameMode.ADVENTURE);
                return;
            }
            setGameMode(getUser(args[0]), GameMode.ADVENTURE);
            return;
        }
        if(cl.equalsIgnoreCase("gmsp", "gamemodesp")) {
            if(args.length == 0) {
                specifyUser();
                setGameMode(s.getUser(), GameMode.SPECTATOR);
                return;
            }
            setGameMode(getUser(args[0]), GameMode.SPECTATOR);
            return;
        }
        if(args.length == 1) {
            hasPermission("gamemode");
            specifyUser();
            setGameMode(s.getUser(), getGamemode(args[0]));
            return;
        }
        if(args.length == 2) {
            hasPermission("gamemode.other");
            specifyUser();
            User t = getUser(args[1]);
            if(t.getName() == s.getName()) {
                setGameMode(t, getGamemode(args[0]));
                return;
            }
            setGameMode(t, getGamemode(args[0]));
        }
    }
    /*
    User u = getUser(args[1]);
    GameMode gm = getGamemode(args[0]);
            if(u.getBase().getGameMode() == gm) {
        sendMessage("GameMode.Target.alreadyIs", u.getName(), StringUtils.getNameFromEnum(gm));
        return;
    }
            u.getBase().setGameMode(gm);
    sendMessage("GameMode.Target.sender", u.getName(), StringUtils.getNameFromEnum(gm));
            if(getBoolean("Commands.GameMode.Target.informTarget"))
    sendMessage("GameMode.Target.target");
    */
    void setGameMode(User u, GameMode gm) throws CommandException {
        if(u.getName()==s.getName()) {
            hasPermission("gamemode");
            specifyUser();
            if(u.getBase().getGameMode() == gm) {
                sendMessage("GameMode.alreadyIs", StringUtils.getNameFromEnum(gm));
                return;
            }
            u.getBase().setGameMode(gm);
            sendMessage("GameMode.set", StringUtils.getNameFromEnum(gm));
            return;
        }
        hasPermission("gamemode.other");
        specifyUser();
        if(u.getBase().getGameMode() == gm) {
            sendMessage("GameMode.Target.alreadyIs", u.getName(), StringUtils.getNameFromEnum(gm));
            return;
        }
        u.getBase().setGameMode(gm);
        sendMessage("GameMode.Target.sender", u.getName(), StringUtils.getNameFromEnum(gm));
        if(getBoolean("Commands.GameMode.Target.informTarget"))
            sendMessage("GameMode.Target.target");
    }
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException, IllegalArgumentException {
        List<String> f = Lists.newArrayList();
        if(label.equalsIgnoreCase("gamemode", "gm")) {
            f.addAll(loadTab(1, ListsHelper.createList("creative", "survival", "spectator", "adventure")));
            f.addAll(loadTab(2, CoreAPI.findNames(CoreAPI.getOnlineUsers())));
        }else {
            f.addAll(loadTab(1, CoreAPI.findNames(CoreAPI.getOnlineUsers())));
        }
        
        return f;
    }
}
