package me.perryplaysmc.plugin.commands.moderator;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 8/31/19-2023
 * Package: net.core.plugin.commands.moderator
 * Path: CommandCommandSpy
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandCommandSpy extends Command {
    
    public CommandCommandSpy() {
        super("commandspy", "cmdspy");
        useMaxTab=true;
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(args.length == 0) {
            hasPermission("commandspy");
            sendMessage("Commands.CommandSpy.self", s.isCommandSpyEnabled() ? "disabled" : "enabled" );
            s.toggleCommandSpy();
            return;
        }
        hasPermission("commandspy.other");
        User u = getUser(args[0]);
        if(u.getName()==s.getName()) {
            run(s, commandLabel, new Argument[]{});
            return;
        }
        sendMessage("Commands.CommandSpy.other", u.getName(), u.isCommandSpyEnabled() ? "disabled" : "enabled");
        u.toggleCommandSpy();
    }
}
