package me.perryplaysmc.plugin.commands.admin;

import me.perryplaysmc.chat.FancyMessage;
import me.perryplaysmc.command.*;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.core.configuration.ConfigManager;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.util.ChatPaginator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/3/19-2023
 * Package: me.perryplaysmc.sparrow.plugin.commands.admin
 * Path: CommandMain
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandMain extends Command {
    
    public CommandMain() {
        super(CoreAPI.getSettings().getStringList("settings.main-command"));
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("reload")) {
                hasPermission(getName().toLowerCase()+".config.reload.all");
                for(Config cfg : ConfigManager.getConfigs()) {
                    if(fromString(cfg.getLocalName())==null)
                        cfg.reload();
                }
                sendMessage("Commands.Main.reload.all");
                return;
            } else if(args[0].equalsIgnoreCase("info")) {
                hasPermission(getName().toLowerCase()+".info");
                getInfo(0);
                return;
            }else if(args[0].equalsIgnoreCase("reset")) {hasPermission(getName().toLowerCase()+".config.reload.all");
                for(Config cfg : ConfigManager.getConfigs()) {
                    if(fromString(cfg.getLocalName())==null)
                        cfg.resetConfig();
                }
                sendMessage("Commands.Main.reset.all");
                return;
            }
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("reset")) {
                hasPermission(getName().toLowerCase()+".config.reset");
                Config cfg = getConfigMain(args[1]);
                cfg.resetConfig();
                sendMessage("Commands.Main.reset.single", cfg.getLocalName());
                return;
            }
            else if(args[0].equalsIgnoreCase("reload")) {
                hasPermission(getName().toLowerCase()+".config.reload");
                Config cfg = getConfigMain(args[1]);
                cfg.reload();
                sendMessage("Commands.Main.reload.single", cfg.getLocalName());
                return;
            }
            else if(args[0].equalsIgnoreCase("info")) {
                hasPermission(getName().toLowerCase()+".info");
                getInfo(integer(args[1]));
                return;
            }
        }
        if(args.length > 2) {
        
        }
    }
    
    private void getInfo(int index) {
        String spaces = "";
        String f = CoreAPI.getMessages().getString("Commands.Main.Info.footer");
        if(!StringUtils.isEmpty(f))
            if(f.contains("-")) {
                for(int i = 0; i < CoreAPI.getSettings().getString("settings.main-name").length();i++) {
                    spaces+="-";
                }
            }
            else if(f.contains(" ")) {
                for(int i = 0; i < CoreAPI.getSettings().getString("settings.main-name").length();i++) {
                    spaces+=" ";
                }
            }
        int maxWidth = f.toCharArray().length;
        FancyMessage msg = new FancyMessage(CoreAPI.format("Commands.Main.Info.header", CoreAPI.getSettings().getString("settings.main-name")));
        String toPageinate = CommandHandler.getCommands().get(0).getName();
        for(Command cmd : CommandHandler.getCommands()) {
            if(cmd!= CommandHandler.getCommands().get(0))
                toPageinate+="\n"+cmd.getName();
        }
        if(index < 1) { index=1; }
        if(index > StringUtils.getPages(toPageinate)) { index = StringUtils.getPages(toPageinate); }
        ChatPaginator.ChatPage page = ChatPaginator.paginate(toPageinate, index, Integer.MAX_VALUE, 10);
        if(CoreAPI.getMessages().getBoolean("Commands.Main.Info.page.click.allowed")) {
            String p1 = CoreAPI.getMessages().getString("Commands.Main.Info.page.click.previous");
            String p2 = CoreAPI.getMessages().getString("Commands.Main.Info.page.click.next");
            String t = CoreAPI.format("Commands.Main.Info.page.click.text", page.getPageNumber(), page.getTotalPages());
            if((index+-1) < 1) {
                p1 = "§7"+ColorUtil.removeColor(p1);
            }
            msg.then("\n"+StringUtils.centerText(maxWidth,""));
            msg.then(p1);
            if((index+-1) > 0) msg.command("/" + getName() + " info " + (index+-1));
            msg.then(t);
            if((index+1) > page.getTotalPages()) {
                p2 = "§7"+ColorUtil.removeColor(p2);
            }
            msg.then(p2);
            if((index+1) <= page.getTotalPages()) msg.command("/" + getName() + " info " + (index+1));
        }else
            msg.then("\n"+StringUtils.centerText(maxWidth, CoreAPI.format("Commands.Main.Info.page.text", page.getPageNumber(), page.getTotalPages())));
        List<String> x = new ArrayList<>();
        for(int i = 0; i < page.getLines().length; i++) {
            String s = page.getLines()[i];
            if(x.contains(s))continue;
            else x.add(s);
            Command cmd = CommandHandler.getCommand(ColorUtil.removeColor(s));
            msg.then("\n"+CoreAPI.format("Commands.Main.Info.format", cmd.getName(),
                    cmd.getAliasesAsList().size()> 0 ? StringUtils.formatList(false, true, cmd.getAliasesAsList()) : "None",
                    cmd.getDescription(),
                    cmd.getDefaultPermissions().length> 0 ? StringUtils.formatList(false, true, Arrays.asList(cmd.getDefaultPermissions())) : "None",
                    cmd.getPermission(),
                    cmd.getUsage()));
            
            if(!StringUtils.isEmptyArray(CoreAPI.getMessages().getStringArray("Commands.Main.Info.hover"))) {
                msg.hover(CoreAPI.formatArray("Commands.Main.Info.hover", cmd.getName(),
                        cmd.getAliasesAsList().size()> 0 ? StringUtils.formatList(false, true, cmd.getAliasesAsList()) : "None",
                        cmd.getDescription(),
                        cmd.getDefaultPermissions().length> 0 ? StringUtils.formatList(false, true, Arrays.asList(cmd.getDefaultPermissions())) : "None",
                        cmd.getPermission(),
                        cmd.getUsage()));
            }
            
            if(CoreAPI.getMessages().getBoolean("Commands.Main.Info.runCommand")) {
                msg.command(CoreAPI.format("Commands.Main.Info.command", cmd.getName()));
            }else {
                msg.suggest(CoreAPI.format("Commands.Main.Info.command", cmd.getName()));
            }
        }
        
        msg.then("\n" + CoreAPI.format("Commands.Main.Info.footer", spaces));
        msg.send(s.getBase());
    }
    
    
    public UUID fromString(String name) {
        String[] components = name.split("-");
        if (components.length != 5)
            return null;
        for (int i=0; i<5; i++)
            components[i] = "0x"+components[i];
        
        long mostSigBits = Long.decode(components[0]).longValue();
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(components[1]).longValue();
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(components[2]).longValue();
        
        long leastSigBits = Long.decode(components[3]).longValue();
        leastSigBits <<= 48;
        leastSigBits |= Long.decode(components[4]).longValue();
        
        return new UUID(mostSigBits, leastSigBits);
    }
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException, IllegalArgumentException {
        List<String> ret = new ArrayList<>();
        ret.addAll(loadTab(1, Arrays.asList("reload", "reset", "info")));
        ret.addAll(loadTab(2, "reset", CoreAPI.findNames(ConfigManager.getConfigs())));
        ret.addAll(loadTab(2, "reload", CoreAPI.findNames(ConfigManager.getConfigs())));
        return ret;
    }
}
