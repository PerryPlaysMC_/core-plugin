package me.perryplaysmc.plugin.commands.admin.management;

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Info;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.permissions.group.Group;
import me.perryplaysmc.permissions.group.GroupManager;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.ListsHelper;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.util.ChatPaginator;
import org.jetbrains.annotations.NotNull;

import java.util.*;

@SuppressWarnings("all")
public class CommandPermission extends Command {
    
    HashMap<CommandSource, TabDataHolder> tabData = new HashMap<>();
    
    List<CommandSource> tabData2 = new ArrayList<>();
    
    public CommandPermission() {
        super("permission", "permissions", "perm", "perms");
        addDefaultPermission("permission.*");
        setDescription("Handles all Permissions");
    }
    
    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        if(args.length == 0) {
            sendHelp("");
            return;
        }
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("reload")) {
                hasPermission("permission.reload");
                CoreAPI.getPermissions().reload();
                GroupManager.reload();
                sendMessage("Commands.Permission.reload");
            }else sendHelp("");
            return;
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("help", "?")) {
                sendHelp(args[1].getContents());
                return;
            }
        }
        if(args.length == 3) {
            if(args[0].equalsIgnoreCase("group")) {
                if(args[2].equalsIgnoreCase("create")) {
                    if(tabData.containsKey(s) && tabData.get(s).type == "createGroup")
                        tabData.remove(s);
                    hasPermission("permission.group.create");
                    checkGroup(true, args[1]);
                    Group g = new Group(args[1].getContents(), new ArrayList<>());
                    sendMessage("Commands.Permission.Group.Create", g.getName());
                }else if(args[2].equalsIgnoreCase("delete")) {
                    hasPermission("permission.group.delete");
                    checkGroup(false, args[1]);
                    Group g = getGroup(args[1]);
                    g.delete();
                    sendMessage("Commands.Permission.Group.Delete", g.getName());
                }else if(args[2].equalsIgnoreCase("info")) {
                    hasPermission("permission.group.info");
                    Group g = getGroup(args[1]);
                    getGroup(1, g);
                }else if(args[2].equalsIgnoreCase("removePrefix")) {
                    hasPermission("permission.group.set.prefix");
                    Group g = getGroup(args[1]);
                    g.setPrefix(g.uuid.toString());
                    sendMessage("Commands.Permission.Group.remove-Prefix", g.getName());
                }else if(args[2].equalsIgnoreCase("removeSuffix")) {
                    hasPermission("permission.group.set.suffix");
                    Group g = getGroup(args[1]);
                    g.setSuffix(g.uuid.toString());
                    sendMessage("Commands.Permission.Group.remove-Suffix", g.getName());
                }else sendHelp("");
            }
            else if(args[0].equalsIgnoreCase("user")) {
                if(args[2].equalsIgnoreCase("info")) {
                    hasPermission("permission.user.info");
                    OfflineUser u = getAnyUser(args[1]);
                    getUser(1, u);
                }else if(args[2].equalsIgnoreCase("removePrefix")) {
                    hasPermission("permission.group.set.prefix");
                    OfflineUser u = getAnyUser(args[1]);
                    u.getPermissionData().setPrefix(u.getUniqueId().toString());
                    sendMessage("Commands.Permission.User.remove-Prefix", u.getName());
                }else if(args[2].equalsIgnoreCase("removeSuffix")) {
                    hasPermission("permission.group.set.suffix");
                    OfflineUser u = getAnyUser(args[1]);
                    u.getPermissionData().setSuffix(u.getUniqueId().toString());
                    sendMessage("Commands.Permission.User.remove-Suffix", u.getName());
                }else sendHelp("");
            }else sendHelp("");
            return;
        }
        if(args.length >= 3) {
            if(args[0].equalsIgnoreCase("group")) {
                if(args[2].equalsIgnoreCase("setprefix")) {
                    hasPermission("permission.group.set.prefix");
                    Group g = getGroup(args[1]);
                    String newPrefix = createMessage(3);
                    g.setPrefix(newPrefix);
                    sendMessage("Commands.Permission.Group.Set-Prefix", g.getName(), ColorUtil.translateChatColor(newPrefix));
                    return;
                }else if(args[2].equalsIgnoreCase("setsuffix")) {
                    hasPermission("permission.group.set.suffix");
                    Group g = getGroup(args[1]);
                    String newSuffix = createMessage(3);
                    g.setSuffix(newSuffix);
                    sendMessage("Commands.Permission.User.Set-Suffix", g.getName(), ColorUtil.translateChatColor(newSuffix));
                    return;
                }
            } else
            if(args[0].equalsIgnoreCase("user")) {
                if(args[2].equalsIgnoreCase("setprefix")) {
                    hasPermission("permission.user.set.prefix");
                    OfflineUser u = getAnyUser(args[1]);
                    String newPrefix = createMessage(3);
                    u.getPermissionData().setPrefix(newPrefix);
                    sendMessage("Commands.Permission.User.Set-Prefix", u.getName(), ColorUtil.translateChatColor(newPrefix));
                    return;
                }else if(args[2].equalsIgnoreCase("setsuffix")) {
                    hasPermission("permission.user.set.suffix");
                    OfflineUser u = getAnyUser(args[1]);
                    String newSuffix = createMessage(3);
                    u.getPermissionData().setSuffix(newSuffix);
                    sendMessage("Commands.Permission.User.Set-Suffix", u.getName(), ColorUtil.translateChatColor(newSuffix));
                    return;
                }
            }
        }
        if(args.length == 4) {
            if(args[0].equalsIgnoreCase("user")) {
                if(args[2].equalsIgnoreCase("info")) {
                    hasPermission("permission.user.info");
                    OfflineUser u = getAnyUser(args[1]);
                    int index = integer(args[3]);
                    getUser(index, u);
                }
            }else
            if(args[0].equalsIgnoreCase("group")) {
                if(args[2].equalsIgnoreCase("info")) {
                    hasPermission("permission.group.info");
                    Group g = getGroup(args[1]);
                    int index = integer(args[3]);
                    getGroup(index, g);
                }else if(args[2].equalsIgnoreCase("rename")) {
                    hasPermission("permission.group.rename");
                    Group g = getGroup(args[1]);
                    String name = g.getName();
                    checkGroup(true, args[3]);
                    g = g.rename(args[3].getContents());
                    sendMessage("Commands.Permission.Group.Rename", name, g.getName());
                }else if(args[2].equalsIgnoreCase("default")) {
                    hasPermission("permission.group.default");
                    Group g = getGroup(args[1]);
                    boolean def = getBoolean(args[3]);
                    g.setDefault(def);
                    sendMessage("Commands.Permission.Group.Default", g.getName(), def);
                }
            }else sendHelp("");
            return;
        }
        if(args.length == 5) {
            if(args[0].equalsIgnoreCase("group")) {
                Group g = getGroup(args[1]);
                if(args[2].equalsIgnoreCase("user")) {
                    OfflineUser u = getAnyUser(args[4]);
                    if(args[3].equalsIgnoreCase("remove")) {
                        hasPermission("permission.group.user.remove");
                        hasGroup(true, u, g);
                        g.removeUser(u);
                        sendMessage("Commands.Permission.Group.User.Remove", u.getName(), g.getName());
                    }else if(args[3].equalsIgnoreCase("add")) {
                        hasPermission("permission.group.user.add");
                        hasGroup(false, u, g);
                        g.addUser(u);
                        sendMessage("Commands.Permission.Group.User.Add", u.getName(), g.getName());
                    }
                }else if(args[2].equalsIgnoreCase("inherits")) {
                    Group u = getGroup(args[4]);
                    if(args[3].equalsIgnoreCase("remove")) {
                        hasPermission("permission.group.inherit.remove");
                        hasGroup(true, u, g);
                        g.removeInheritance(u);
                        g.updateInheritance();
                        sendMessage("Commands.Permission.Group.Remove-Inheritance", u.getName(), g.getName());
                    }else if(args[3].equalsIgnoreCase("add")) {
                        hasPermission("permission.group.inherit.add");
                        hasGroup(false, u, g);
                        g.addInheritance(u);
                        g.updateInheritance();
                        sendMessage("Commands.Permission.Group.Add-Inheritance", u.getName(), g.getName());
                    }
                }else if(args[2].equalsIgnoreCase("permission", "perm")) {
                    if(args[3].equalsIgnoreCase("add")) {
                        hasPermission("permission.group.permission.add");
                        hasPermission(true, g, args[3].getContents());
                        g.addPerm(args[4].getContents());
                        sendMessage("Commands.Permission.Group.Add-Permission", args[4].getContents(), g.getName());
                    }else if(args[3].equalsIgnoreCase("remove")) {
                        hasPermission("permission.group.permission.remove");
                        hasPermission(false, g, args[4].getContents());
                        g.removePerm(args[4].getContents());
                        sendMessage("Commands.Permission.Group.Remove-Permission", args[4].getContents(), g.getName());
                    }else if(args[3].equalsIgnoreCase("check")) {
                        hasPermission("permission.group.check");
                        sendMessage("Commands.Permission.Group.check", g.getName(), g.hasPermission(args[4].getContents()),
                                g.isPermissionSet(args[4].getContents()));
                    }else sendHelp("");
                    
                }
            } else if(args[0].equalsIgnoreCase("user")) {
                OfflineUser u = getAnyUser(args[1]);
                if(args[2].equalsIgnoreCase("permission", "perm")) {
                    if(args[3].equalsIgnoreCase("add")) {
                        hasPermission("permission.user.permission.add");
                        hasPermission(true, u, args[4].getContents());
                        u.getPermissionData().addPermission(args[4].getContents());
                        sendMessage("Commands.Permission.User.Add-Permission", args[4].getContents(), u.getName());
                    }else if(args[3].equalsIgnoreCase("remove")) {
                        hasPermission("permission.user.permission.remove");
                        hasPermission(false, u, args[4].getContents());
                        u.getPermissionData().removePermission(args[4].getContents());
                        sendMessage("Commands.Permission.User.Remove-Permission", args[4].getContents(), u.getName());
                    }else if(args[3].equalsIgnoreCase("check")) {
                        hasPermission("permission.user.check");
                        sendMessage("Commands.Permission.User.check", u.getName(), u.getPermissionData()
                                        .hasSinglePermission(args[4].getContents()),
                                u.getPermissionData().isSinglePermissionSet(args[4].getContents()));
                    }else sendHelp("");
                    
                }
            }
            return;
        }if(args.length>5)
            argsLengthError(true);
        else argsLengthError(false);
    }
    
    
    
    private void getUser(int index, OfflineUser u) {
        
        dataHolder d = userPerms(u);
        String sb = d.getString();
        HashMap<String, String> per = d.getMap();
        if(index < 1) { index=1; }
        if(index > StringUtils.getPages(sb)) { index = StringUtils.getPages(sb); }
        ChatPaginator.ChatPage page = ChatPaginator.paginate(sb, index, Integer.MAX_VALUE, 10);
        
        Message msg = new Message("[c]User>> [pc]"+u.getRealName()+"[c]'s [c]Info");
        msg.then("\n").then("[c]Prefix: [pc]" + (u.getPermissionData().getPrefix().isEmpty() ? "Not Set" : u.getPermissionData().getPrefix()));
        msg.then("\n").then("[c]Suffix: [pc]" + (u.getPermissionData().getSuffix().isEmpty() ? "Not Set" : u.getPermissionData().getSuffix()));
        Group g1 = u.getPermissionData().getPrimaryGroup();
        msg.then("\n").then("[c]Primary Group: [pc]" + (g1 == null ? "Not set" : g1.getName()));
        if(!StringUtils.isEmpty(sb)) {
            msg.then("\n").then("[c]Permissions([pc]" + index + "[c]/[pc]" + StringUtils.getPages(sb) + "[c]):");
            for (String a : page.getLines()) {
                String c = ColorUtil.removeColor(a);
                msg.then("\n").then(a.substring(a.indexOf("-") + 1))
                        .tooltip(
                                "[c]Click to remove permission: ",
                                "[pc]" + per.get(c.substring(0, c.indexOf("-") + 1)))
                        .command("/permission user " + u.getName() + " permission remove " + (per.get(c.substring(0, c.indexOf("-") + 1))));
            }
        }else msg.then("\n").then("[c]Permissions: [pc]not set");
        int x = 1;
        if(u.getPermissionData().getGroups().size() > 0) {
            msg.then("\n").then("[c]Groups: ");
            for (Group g : u.getPermissionData().getGroups()) {
                if(g==null||g.getName()==null||StringUtils.isEmpty(g.getName()))continue;
                
                msg.then("\n").then("[c] ([pc]" + x + "[c])- [pc]" + g.getName())
                        .tooltip(
                                "[c]Rank: [pc]" + g.getRank(),
                                "[c]Prefix: [pc]" + (g.getPrefix().isEmpty() ? "Prefix Not set" : g.getPrefix()),
                                "[c]Suffix: [pc]" + (g.getSuffix().isEmpty() ? "Suffix Not set" : g.getSuffix()),
                                "[c]Click to remove"
                        ).command("/permission group " + g.getName() + " user " + u.getName() + " remove");
                x++;
            }
        }
        else msg.then("\n").then("[c]Groups: [pc]not set");
        msg.send(s);
    }
    
    private void getGroup(int index, Group g) {
        dataHolder d = groupPerms(g);
        String sb = d.getString();
        HashMap<String, String> per = d.getMap();
        if(index < 1) { index=1; }
        if(index > StringUtils.getPages(sb)) { index = StringUtils.getPages(sb); }
        ChatPaginator.ChatPage page = ChatPaginator.paginate(sb, index, Integer.MAX_VALUE, 10);
        
        Message msg = new Message("[c]Group>> [pc]"+g.getName()+"[c]'s [c]Info");
        msg.then("\n").then("[c]Prefix: [pc]" + (g.getPrefix().isEmpty() ? "Not Set" : g.getPrefix()));
        msg.then("\n").then("[c]Suffix: [pc]" + (g.getSuffix().isEmpty() ? "Not Set" : g.getSuffix()));
        msg.then("\n").then("[c]Default: [pc]" + g.isDefault());
        if(!StringUtils.isEmpty(sb)) {
            msg.then("\n").then("[c]Permissions([pc]" + index + "[c]/[pc]" + StringUtils.getPages(sb)+"[c]):");
            for (String a : page.getLines()) {
                String c = ColorUtil.removeColor(a);
                msg.then("\n").then(a.substring(a.indexOf("-") + 1))
                        .tooltip(
                                "[c]Click to remove permission: ",
                                "[pc]" + per.get(c.substring(0, c.indexOf("-") + 1)))
                        .command("/permission group " + g.getName() + " permission remove " + (per.get(c.substring(0, c.indexOf("-") + 1))) + " remove");
            }
        } else msg.then("\n").then("[c]Permissions: [pc]Not set");
        int x = 1;
        if(g.getInherits().size() > 0)
            for(Group g1 : g.getInherits()) {
                
                msg.then("\n").then("[c] ([pc]" + x + "[c])- [pc]" + g.getName())
                        .tooltip(
                                "[c]Rank: [pc]" + g1.getRank(),
                                "[c]Prefix: [pc]" + g1.getPrefix(),
                                "[c]Click to remove"
                        ).command("/permission group " + g1.getName() + " inherits " + g1.getName() + " remove");
                x++;
            }
        else msg.then("\n").then("[c]Groups: [pc]not set");
        msg.send(s);
    }
    
    private void sendHelp(@NotNull String type) {
        if(!type.equalsIgnoreCase("user")&&!type.equalsIgnoreCase("group")) {
            Message msg = new Message("[c]/[pc]permission [r][help/?] [a]<group/user>");
            msg.suggest("/permission help ");
            msg.send(s);
            return;
        }
        Info info = new Info(this, s);
        if(type.equalsIgnoreCase("user")) {
            info.addSub(new String[]{"permission.reload"}
                    , "reload")
                    .tooltip("[c]Reloads the configurations",
                            "[c]Click to run: /[pc]permission [a]reload")
                    .command("/permission reload");
            
            info.addSub(new String[]{"permission.user.info"}
                    , "getuser [a]<user>")
                    .tooltip("[c]Gets User Information",
                            "[c]Click to insert: /[pc]permission [r]getuser [a]<user>")
                    .suggest("/permission getuser <user>");
            
            info.addSub(new String[]{"permission.user.permission.add"}
                    , "user [a]<user> [o]permission [a]<permission> [o]add")
                    .tooltip("[c]Grants user Permission",
                            "[c]Click to insert: /[pc]permission [r]user [a]<user> [o]permission [a]<permission> [o]add")
                    .suggest("/permission user <user> permission <permission> add");
            
            info.addSub(new String[]{"permission.user.permission.remove"}
                    , "user [a]<user> [o]permission [a]<permission> [o]remove")
                    .tooltip("[c]Revokes user Permission",
                            "[c]Click to insert: /[pc]permission [r]user [a]<user> [o]permission [a]<permission> [o]remove")
                    .suggest("/permission user <user> permission <permission> remove");
            
            info.addSub(new String[]{"permission.user.set.prefix"}
                    , "user [a]<user> [o]setprefix [a]<...newPrefix>")
                    .tooltip("[c]Sets users Prefix",
                            "[c]Click to insert: /[pc]permission [r]user [a]<user> [o]setprefix [a]<...newPrefix>")
                    .suggest("/permission user <user> setprefix <...newPrefix>");
            
            info.addSub(new String[]{"permission.user.set.suffix"}
                    , "user [a]<user> [o]setsuffix [a]<...newSuffix>")
                    .tooltip("[c]Sets users Suffix",
                            "[c]Click to insert: /[pc]permission [r]user [a]<user> [o]setsuffix [a]<...newSuffix>")
                    .suggest("/permission user <user> setsuffix <...newSuffix>");
        }
        if(type.equalsIgnoreCase("group")) {
            info.addSub(new String[]{"permission.group.create"}
                    , "[r]group [a]<group> [o]create")
                    .tooltip("[c]Creates a Group", "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]create")
                    .suggest("/permission group <group> create");
            
            info.addSub(new String[]{"permission.group.delete"}
                    , "[r]group [a]<group> [o]delete")
                    .tooltip("[c]Deletes a Group", "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]delete")
                    .suggest("/permission group <group> delete");
            
            info.addSub(new String[]{"permission.group.rename"}
                    , "[r]group [a]<group> [o]rename [a]<newName>")
                    .tooltip("[c]Renames a Group", "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]rename [a]<newName>")
                    .suggest("/permission group <group> rename <newName>");
            
            info.addSub(new String[]{"permission.group.default"}
                    , "[r]group [a]<group> [o]default [a]<true/false>")
                    .tooltip("[c]Set a group to default", "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]default [a]<true/false>")
                    .suggest("/permission group <group> default <true/false>");
            
            info.addSub(new String[]{"permission.group.permission.add"}
                    , "group [a]<group> [o]permission [a]<permission> [o]add")
                    .tooltip("[c]Gives Permission to specified group",
                            "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]permission [a]<permission> [o]add")
                    .suggest("/permission group <group> permission <permission> add");
            
            info.addSub(new String[]{"permission.group.permission.remove"}
                    , "group [a]<group> [o]permission [a]<permission> [o]remove")
                    .tooltip("[c]Removes Permission from specified group",
                            "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]permission [a]<permission> [o]remove")
                    .suggest("/permission group <group> permission <permission> remove");
            
            info.addSub(new String[]{"permission.group.set.prefix"}
                    , "group [a]<group> [o]setprefix [a]<...newPrefix>")
                    .tooltip("[c]Sets groups Prefix",
                            "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]setprefix [a]<...newPrefix>")
                    .suggest("/permission group <group> setprefix <...newPrefix>");
            
            info.addSub(new String[]{"permission.group.set.suffix"}
                    , "group [a]<group> [o]setsuffix [a]<...newSuffix>")
                    .tooltip("[c]Sets groups Suffix",
                            "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]setsuffix [a]<...newSuffix>")
                    .suggest("/permission group <group> setsuffix <...newSuffix>");
            
            info.addSub(new String[]{"permission.group.user.add"}
                    , "[r]group [a]<group> [o]user [a]<user> [o]add")
                    .tooltip("[c]Adds user to the group", "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]user [a]<user> [o]add")
                    .suggest("/permission group <group> user <user> add");
            
            info.addSub(new String[]{"permission.group.user.remove"}
                    , "[r]group [a]<group> [o]user [a]<user> [o]remove")
                    .tooltip("[c]Removes user from the group", "[c]Click to insert: /[pc]permission [r]group [a]<group> [o]user [a]<user> [o]remove")
                    .suggest("/permission group <group> user <user> remove");
        }
        
        info.send(s);
    }
    
    
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) {
        List<String> ret = Lists.newArrayList();
        Group g = null;
        if(args.length>=2)
            g = GroupManager.getGroup(args[1].getContents());
        OfflineUser u = null;
        if(args.length>=2)
            u = CoreAPI.getAllUser(args[1].getContents());
        ret.addAll(loadTab(1, Arrays.asList("group", "user", "reload", "help")));
        ret.addAll(loadTab(2, new ArgumentData(0, "?", "help"), Arrays.asList("group", "user")));
        ret.addAll(loadTab(2, "group", CoreAPI.findNames(GroupManager.getGroups())));
        ret.addAll(loadTab(3, new ArgumentData(0, "group"),args.length >= 2 && GroupManager.getGroup(args[1].getContents()) == null, Arrays.asList("create")));
        //Group arg 0
        if(args[0].equalsIgnoreCase("group")&&g != null)
            ret.addAll(loadTab(3,
                    Arrays.asList("delete", "permission", "user", "rename", "default", "setprefix", "setsuffix", "info", "inherits", "removeprefix","removesuffix")));
        //User
        ret.addAll(loadTab(2, "user",
                CoreAPI.findNames(CoreAPI.getAllUsers())));
        //User
        ret.addAll(loadTab(3, new ArgumentData(0, "user"),
                Arrays.asList("permission", "setprefix", "setsuffix", "info", "removeprefix","removesuffix")));
        
        ret.addAll(loadTab(4, "default",
                ListsHelper.createList("true", "false")));
        ret.addAll(loadTab(4, new ArgumentData(new int[]{0,2}, "group", "info"),
                g!=null?forInt(1,StringUtils.getPages(groupPerms(g).getString())+1):Lists.newArrayList()));
        
        ret.addAll(loadTab(4, new ArgumentData(2, "user"),
                ListsHelper.createList("add", "remove")));
        ret.addAll(loadTab(5, new ArgumentData(new int[]{2, 3}, "user", "add"),
                ListsHelper.createList(CoreAPI.findNames(CoreAPI.getAllUsers())).removeAll(g!=null? g.getUsers():Lists.newArrayList())));
        ret.addAll(loadTab(5, new ArgumentData(new int[]{2, 3}, "user", "remove"),
                ListsHelper.createList(g!=null? g.getUsers():Lists.newArrayList())));
        
        ret.addAll(loadTab(4, "permission",
                ListsHelper.createList("add", "remove", "check")));
        ret.addAll(loadTab(4, new ArgumentData(new int[]{0,2}, "user", "info"),
                u!=null?forInt(1,StringUtils.getPages(userPerms(u).getString())+1):Lists.newArrayList()));
        //User permission
        ret.addAll(loadTab(5, new ArgumentData(new int[]{0,2,3}, "user", "permission", "add"),
                ListsHelper.createList(CoreAPI.findNames(Bukkit.getPluginManager().getPermissions())).removeAll(u!=null?u.getPermissionData().getPermissions():Lists.newArrayList())));
        
        ret.addAll(loadTab(5, new ArgumentData(new int[]{2,3}, "permission", "check"),
                ListsHelper.createList(CoreAPI.findNames(Bukkit.getPluginManager().getPermissions()))));
        
        ret.addAll(loadTab(5, new ArgumentData(new int[]{0,2,3}, "user", "permission", "remove"),
                u!=null?u.getPermissionData().getPermissions():Lists.newArrayList()));
        //Group permission
        ret.addAll(loadTab(5, new ArgumentData(new int[]{0,2,3}, "group", "permission", "add"),
                ListsHelper.createList(CoreAPI.findNames(Bukkit.getPluginManager().getPermissions())).removeAll(g!=null?g.getPermissions():Lists.newArrayList())));
        ret.addAll(loadTab(5, new ArgumentData(new int[]{0,2,3}, "group", "permission", "remove"),
                ListsHelper.createList(g!=null?g.getPermissions():Lists.newArrayList())));
        ret.addAll(loadTab(4, new ArgumentData(2, "inherits"), Arrays.asList("add", "remove")));
        ret.addAll(loadTab(5, new ArgumentData(new int[]{2, 3}, "inherits", "add"),
                g!=null?ListsHelper.createList(CoreAPI.findNames(GroupManager.getGroups())).remove(g.getName()):Lists.newArrayList()));
        ret.addAll(loadTab(5, new ArgumentData(new int[]{2, 3}, "inherits", "remove"),
                g!=null?ListsHelper.createList(CoreAPI.findNames(g.getInherits())):Lists.newArrayList()));
        
        return ret;
    }
    
    
    private dataHolder userPerms(OfflineUser u) {
        List<String> perms = new ArrayList<>();
        for(PermissionAttachmentInfo info : u.getPermissionData().getEffectivePermissions()) {
            if(!perms.contains(info.getPermission()) && info.getValue()
               && !info.getPermission().equalsIgnoreCase("craftbukkit")
               && !info.getPermission().equalsIgnoreCase("minecraft")) {
                perms.add(info.getPermission());
            }
        }
        Collections.sort(perms);
        StringBuilder sb = new StringBuilder();
        int e = 1;
        HashMap<String, String> per = new HashMap<>();
        for(String a : perms) {
            sb.append(e+"-"+"[c] ([pc]" + e + "[c])- [pc]" + a + "\n");
            per.put(e+"-", a);
            e++;
        }
        return new dataHolder(sb.toString(), per);
    }
    private dataHolder groupPerms(Group u) {
        List<String> perms = new ArrayList<>();
        for(String info1 : u.getPermissions()) {
            Permission info = Bukkit.getPluginManager().getPermission(info1);
            if(info == null) {
                Bukkit.getPluginManager().addPermission(new Permission(info1));
                info = Bukkit.getPluginManager().getPermission(info1);
            }
            if(!perms.contains(info.getName())
               && !info.getName().equalsIgnoreCase("craftbukkit")
               && !info.getName().equalsIgnoreCase("minecraft")) {
                perms.add(info.getName());
            }
        }
        Collections.sort(perms);
        StringBuilder sb = new StringBuilder();
        int e = 1;
        HashMap<String, String> per = new HashMap<>();
        for(String a : perms) {
            sb.append(e+"-"+"[c] ([pc]" + e + "[c])- [pc]" + a + "\n");
            per.put(e+"-", a);
            e++;
        }
        return new dataHolder(sb.toString(), per);
    }
    
    private class TabDataHolder {
        String name;
        String type;
        public TabDataHolder(String name, String type) {
            this.name = name;
            this.type = type;
        }
    }
    
    private class dataHolder {
        private String s;
        private HashMap<String, String> map;
        public dataHolder(String s, HashMap<String, String> map) {
            this.s = s;
            this.map = map;
        }
        
        public HashMap<String, String> getMap() {
            return map;
        }
        
        public String getString() {
            return s;
        }
    }
    
}
