package me.perryplaysmc.plugin.commands.admin.management;
/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/3/19-2023
 **/

import me.perryplaysmc.chat.Info;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.utils.string.TimeUtil;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.utils.helper.ban.BanData;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.ListsHelper;
import me.perryplaysmc.utils.string.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class CommandBan extends Command {
    
    public CommandBan() {
        super("banish", "blacklist", "black-list", "ban", "banplayer", "banishplayer", "tempban", "tempbanish");
        setDescription("Bans the specified user");
    }
    
    public void run(CommandSource s, CommandLabel cl, Argument[] args) throws CommandException {
        if(!cl.contains("temp")) {
            if(args.length == 0){
                sendHelp();
            }
            else if(args.length == 1) {
                hasPermission("ban");
                OfflineUser ban = getAnyUser(args[0]);
                if(CoreAPI.getBan(ban.getUniqueId())!=null) {
                    sendMessage("Commands.Ban.already-Banned", ban.getRealName());
                    return;
                }
                if(ban.getPermissionData().hasPermission("ban.deny")) {
                    sendMessage("Commands.Ban.ban-Fail");
                    return;
                }
                String reason = StringUtils.translate(CoreAPI.getSettings().getString("settings.DefaultBanReason"));
                CoreAPI.banPlayer(BanData.BanType.NAME, s.getRealName(), ban.getUniqueId(), reason);
                CoreAPI.broadcast(CoreAPI.format("Commands.Ban.default-broadcast", s.getName(), ban.getRealName(), reason));
            } else {
                hasPermission("tempban","ban");
                OfflineUser ban = getAnyUser(args[0]);
                if(CoreAPI.getBan(ban.getUniqueId())!=null) {
                    sendMessage("Commands.Ban.already-Banned", ban.getRealName());
                    return;
                }
                if(ban.getPermissionData().hasPermission("ban.deny")) {
                    sendMessage("Commands.Ban.ban-Fail");
                    return;
                }
                boolean isSilent = false;
                if(args[args.length + -1].equalsIgnoreCase("-s")) {
                    isSilent = true;
                }
                String reason;
                if(args.length == 2)
                    reason = StringUtils.translate(CoreAPI.getSettings().getString("settings.DefaultBanReason"));
                else reason = ColorUtil.translateChatColor(createMessage(1, args.length+-1));
                CoreAPI.banPlayer(BanData.BanType.NAME, s.getRealName(), ban.getUniqueId(), reason);
                if(!isSilent)
                    CoreAPI.broadcast(CoreAPI.format("Commands.Ban.default-broadcast", s.getName(), ban.getRealName(), reason));
                else
                    CoreAPI.broadcastPerm("ban.silent", CoreAPI.format("Commands.Ban.default-broadcast", s.getName(), ban.getRealName(), reason));
            }
        }else {
            if(args.length == 0){
                sendHelp();
            }
            else if(args.length == 1) {
                hasPermission("tempban");
                OfflineUser ban = getAnyUser(args[0]);
                if(CoreAPI.getBan(ban.getUniqueId())!=null) {
                    sendMessage("Commands.Ban.already-Banned", ban.getRealName());
                    return;
                }
                if(ban.getPermissionData().hasPermission("ban.deny")) {
                    sendMessage("Commands.Ban.ban-Fail");
                    return;
                }
                String reason = StringUtils.translate(CoreAPI.getSettings().getString("settings.DefaultBanReason"));
                CoreAPI.banPlayer(BanData.BanType.NAME, s.getRealName(), ban.getUniqueId(), reason);
                CoreAPI.broadcast(CoreAPI.format("Commands.Ban.default-broadcast", s.getName(), ban.getRealName(), reason));
            } else {
                hasPermission("tempban");
                OfflineUser ban = getAnyUser(args[0]);
                if(CoreAPI.getBan(ban.getUniqueId())!=null) {
                    sendMessage("Commands.Ban.already-Banned", ban.getRealName());
                    return;
                }
                if(ban.getPermissionData().hasPermission("ban.deny")) {
                    sendMessage("Commands.Ban.ban-Fail");
                    return;
                }
                boolean isSilent = false;
                if(args[args.length + -1].equalsIgnoreCase("-s")) {
                    isSilent = true;
                }
                long banTimestamp;
                try {
                    banTimestamp = TimeUtil.parseDateDiff(args[args.length + -1].getContents(), true);
                } catch (Exception e) {
                    s.sendMessage("[pc]Error[c]: " + e.getMessage());
                    return;
                }
                String reason;
                if(args.length == 2)
                    reason = StringUtils.translate(CoreAPI.getSettings().getString("settings.DefaultBanReason"));
                else reason = ColorUtil.translateChatColor(createMessage(1, args.length));
                reason = TimeUtil.removeTimePattern(reason).replace("-s", "").trim();
                if(StringUtils.isEmpty(reason))
                    reason = StringUtils.translate(CoreAPI.getSettings().getString("settings.DefaultBanReason"));
                CoreAPI.banPlayer(BanData.BanType.NAME, s.getRealName(), ban.getUniqueId(), reason, banTimestamp);
                if(!isSilent)
                    CoreAPI.broadcast(CoreAPI.format("Commands.Ban.tempban.default-broadcast", s.getName(), ban.getRealName(), reason, TimeUtil.formatDateDiff(banTimestamp)));
                else
                    CoreAPI.broadcastPerm("ban.silent", CoreAPI.format("Commands.Ban.tempban.default-broadcast", s.getName(), ban.getRealName(), reason, TimeUtil.formatDateDiff(banTimestamp)));
            }
        }
    }
    
    
    void sendHelp() {
        Info info = new Info(this);
        info.addSub("[a]<user> [o][reason] [o][-s]")
                .tooltip("[c]Bans player",
                        "[c]Arguments:",
                        " [c]- [a]<user>[c] User to ban",
                        " [c]- [o][reason][c] Reason for ban",
                        " [c]- [o][-s][c] Silent/Public",
                        "[c]Click to insert: /[pc]banish [a]<user> [o][reason] [o][-s]"
                ).suggest("/banish <user> [reason] [-s]");
        info.setName("tempban");
        info.addSub("[a]<user> [o][reason] [o][-s] [a]<time>s,min,h,d,w,m,y")
                .tooltip("[c]Temporarily Bans player",
                        "[c]Arguments: ",
                        " [c]- [a]<user>[c] User to ban",
                        " [c]- [o][reason][c] Reason for ban",
                        " [c]- [o][-s][c] Silent/Public",
                        " [c]- [a]<time>s,min,h,d,w,m,y[c] Time to ban",
                        "[c]Click to insert: /[pc]tempban [a]<user> [o][reason] [o][-s] [a]<time>s,min,h,d,w,m,y"
                ).suggest("/tempban <user> [reason] [-s] <time>s,min,h,d,w,m,y");
        info.send(s);
    }
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        List<String> f = new ArrayList<>();
        f.addAll(loadTab(1, ListsHelper.createList(CoreAPI.findNames(CoreAPI.getAllUsers())).removeAll(CoreAPI.findNames(CoreAPI.getBannedPlayers())).remove(s.getRealName())));
        f.addAll(loadTab(args.length, args.length>1, ListsHelper.createList("-s", "0s", "0min", "0h", "0d", "0w", "0m", "0y")));
        return f;
    }
    
    /*
    msg.then("ban","\n [c]-/[pc]banish[c] [r]<Player> [o][reason] [o][-s][pc]")
                .tooltip("[c]Info: [pc]BanData a player from the server!",
                        "[c]Click to insert:",
                        "[pc]/banish [r]<Player> [o][reason]")
                .suggest("/banish <player> [reason]");
        msg.then("tempban","\n [c]-/[pc]tempban[c] [r]<Player> [o][reason] [o][-s] [r]<time>m,w,d,min[pc]")
                .tooltip("[c]Info: [pc]BanData a player from the server!",
                        "[c]Click to insert:",
                        "[pc]/tempban [r]<Player> [o][reason] [o][-s] [r]<time>m,w,d,min[pc]")
                .suggest("/tempban <player> [reason] [-s] <time>m,w,d,min");
     */
    
}
