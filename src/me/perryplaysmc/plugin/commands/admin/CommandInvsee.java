package me.perryplaysmc.plugin.commands.admin;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.user.handlers.Inventory;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;

public class CommandInvsee extends Command implements Listener {

    private HashMap<Player, Player> p;
    private HashMap<User, Inventory> invens;
    public CommandInvsee() {
        super("inventorysee","isee", "invsee");
        this.setPermission("miniverse.command.invsee");
        this.setDescription("See into anyone's inventory, online or offline");
        this.allowOfflinePlayerTab = true;
        this.useMaxTab = true;
        p = new HashMap<>();
        invens = new HashMap<>();
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        mustBe(true);
        hasPermission("invsee", "inventorysee");
        if(args.length == 1) {
            OfflineUser u = getAnyUser(args[0]);
            s.sendMessage("[c]Opening [pc]" + u.getRealName() + "[c]'s Inventory");
            p.put(s.getUser().getBase(), u.getAsPlayer());
            Inventory i = new Inventory(u);
            invens.put(s.getUser(), i);
            i.openInventory(s.getUser());
        }
    }
    
    @EventHandler
    void onClick(InventoryClickEvent e) {
        User u = CoreAPI.getUser(e.getWhoClicked().getName());
        if(u!=null&&invens.containsKey(u)){
            invens.get(u).onClick(e);
        }
    }
    
    @EventHandler
    void onDrag(InventoryDragEvent e) {
        User u = CoreAPI.getUser(e.getWhoClicked().getName());
        if(u!=null&&invens.containsKey(u)){
            invens.get(u).onDrag(e);
        }
    }
    
    @EventHandler
    void onScroll(PlayerItemHeldEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        if(u!=null&&invens.containsKey(u)){
            invens.get(u).onScroll(e);
        }
    }
    
    @EventHandler
    void onSwap(PlayerSwapHandItemsEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        if(u!=null&&invens.containsKey(u)){
            invens.get(u).onSwap(e);
        }
    }

    @EventHandler
    void onClose(InventoryCloseEvent e) {
        Player s = (Player)e.getPlayer();
        User u = CoreAPI.getUser(s);
        if(p.containsKey(s)){
            Player p1 = Core.getAPI().loadPlayer(Bukkit.getOfflinePlayer(p.get(s).getUniqueId()));
            if(invens.containsKey(u)) {
                PlayerInventory i = invens.get(u).getInv();
                p1.getInventory().setArmorContents(i.getArmorContents());
                p1.getInventory().setContents(i.getContents());
                p1.getInventory().setExtraContents(i.getExtraContents());
                invens.remove(u);
            }
            p1.saveData();
            p.remove(s);
        }
    }
}
