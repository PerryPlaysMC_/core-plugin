package me.perryplaysmc.plugin.commands.admin;

import me.perryplaysmc.command.*;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/20/19-2023
 * Package: me.perryplaysmc.plugin.commands.admin
 * Class: CommandSudo
 * <p>
 * Path: me.perryplaysmc.plugin.commands.admin.CommandSudo
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandSudo extends Command {
    
    public CommandSudo() {
        super("sudo");
        setDescription("Makes the specified user say/run something");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        hasPermission("sudo");
        if(args.length > 1) {
            User t = getUser(args[0]);
            String msg = StringUtils.join(args, " ", 1);
            if(t.getName() != s.getName())
                if(args[1].startsWith("/")) {
                    sendMessage("Sudo.sudo-command", t.getName(), msg);
                }else {
                    sendMessage("Sudo.sudo-chat", t.getName(), msg);
                }
            t.getBase().chat(msg);
        }
    }
    
    org.bukkit.command.Command getCommand(String cl) {
        for(org.bukkit.command.Command cmd : Core.getAPI().getCommandMap().getCommands()) {
            if(cmd.getName().equalsIgnoreCase(cl)) {
                if(cmd.getAliases().contains(cl.toLowerCase())) {
                    return cmd;
                }
            }
        }
        return null;
    }
    
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException, IllegalArgumentException {
        List<String> f = new ArrayList<>();
        f.addAll(loadTab(1, CoreAPI.findNames(CoreAPI.getOnlineUsers())));
        if(args.length == 2 && args[1].getContents().startsWith("/"))
            f.addAll(loadTab(2, CoreAPI.findNames(Core.getAPI().getCommandMap().getCommands()),"/"));
        if(args.length > 2) {
            if(args[1].startsWith("/")) {
                org.bukkit.command.Command cmd = getCommand(args[1].subString(1));
                Command cmd2 = CommandHandler.getCommand(args[1].subString(1));
                if(cmd2!=null) {
                    List<String> f2 = cmd2.tabComplete(s, new CommandLabel(args[1].subString(1)), Arrays.copyOfRange(args, 2, args.length));
                    f.addAll(f2);
                }else if(cmd != null) {
                    String[] arg2 = StringUtils.toStringArray(args);
                    f.addAll(cmd.tabComplete(s.getBase(), args[1].subString(1), Arrays.copyOfRange(arg2, 2, arg2.length)));
            }
            }
        }
        return f;
    }
    
}
