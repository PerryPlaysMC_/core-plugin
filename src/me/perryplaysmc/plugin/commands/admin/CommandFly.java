package me.perryplaysmc.plugin.commands.admin;
/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/1/19-2023
 **/

import me.perryplaysmc.Main_Core;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class CommandFly extends Command {
    
    public CommandFly() {
        super("fly", "flight");
        setDescription("Toggle flight for you or someone else");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(args.length == 0) {
            hasPermission("fly");
            mustBe(true);
            User u = s.getUser();
            boolean fly = u.getBase().getAllowFlight();
            setFly(u);
            sendMessage("Commands.Fly.toggle", !fly ? "enabled" : "disabled", !fly ? "on" : "off");
        }
        if(args.length == 1) {
            hasPermission("fly.other");
            User t = getUser(args[0]);
            if(t.getName() == s.getName()) {
                run(s, commandLabel, new Argument[]{});
                return;
            }
            boolean fly = t.getBase().getAllowFlight();
            setFly(t);
            sendMessage("Commands.Fly.toggle-Other", !fly ? "enabled" : "disabled", t.getName(), !fly ? "on" : "off");
        }
    }
    void setFly(User t) {
        boolean fly = t.getBase().getAllowFlight();
        boolean isFlying = t.getBase().isFlying();
        t.getBase().setAllowFlight(!fly);
        if(!isFlying) {
            if(t.getBase().isOnGround() && !fly) {
                t.getBase().setVelocity(new Vector(0, 0.1, 0));
                (new BukkitRunnable(){
                    @Override
                    public void run() {
                        t.getBase().setFlying(true);
                    }
                }).runTaskLater(Main_Core.getAPI(), 2);
                t.getBase().setFlying(true);
            }
            if(!fly)
                t.getBase().setFlying(true);
        }
    }
}
