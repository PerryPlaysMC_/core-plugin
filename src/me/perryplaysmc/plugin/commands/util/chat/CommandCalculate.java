package me.perryplaysmc.plugin.commands.util.chat;
/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 8/21/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.string.NumberUtil;

@SuppressWarnings("all")
public class CommandCalculate extends Command {
    
    public CommandCalculate() {
        super("calculate", "calc");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(!NumberUtil.containsEquation(args[0].getContents())) {
            sendMessage("Commands.Error.invalid-Equation");
            return;
        }
        sendMessage("Commands.Calculate.calculate", NumberUtil.calc(args[0].getContents()), args[0].getContents());
    }
    
}
