package me.perryplaysmc.plugin.commands.util.chat;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.util.UUIDTypeAdapter;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.UUID;

public class CommandNickName extends Command {
    
    public CommandNickName() {
        super("nickname", "nick");
        setDescription("Change your nickname");
    }
    
    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        hasPermission("nickname");
        if(args.length == 1) {
            mustBe(true);
            User u = s.getUser();
            if(args[0].equalsIgnoreCase("off")) {
                UUID ofID = (u.getUniqueId());
                setSkin(u.getBase(), ofID);
                setName(u, u.getOriginalName());
                sendMessage("Commands.NickName.nick-off", u.getName());
                return;
            }
            OfflinePlayer p = getPlayer(false, args[0]);
            String ofName = (p==null?args[0].getContents():p.getName());
            setName(u, ofName);
            sendMessage("Commands.NickName.nick", ofName);
        }
    }
    void setName(User u, String n) {
        String name = ChatColor.translateAlternateColorCodes('&', n);
        u.setNick(name);
        u.getOriginalBase().setDisplayName(name);
        u.getOriginalBase().setPlayerListName(name);
    }
    
    private ArmorStand createStand(User u, String name) {
        ArmorStand s = u.getWorld().spawn(u.getLocation(), ArmorStand.class);
        s.setCustomName(name);
        s.setCustomNameVisible(true);
        if(name.isEmpty())s.setCustomNameVisible(false);
        s.setVisible(false);
        s.setBasePlate(false);
        s.setMarker(true);
        s.setGravity(false);
        return s;
    }
    
    public static void setSkin(Player player, UUID uuid) {
        GameProfile profile = null;
        try{
            Method getHandle = player.getClass().getMethod("getHandle");
            Object entityPlayer = getHandle.invoke(player);
            boolean gameProfileExists = false;
            try {
                Class.forName("net.minecraft.util.com.mojang.authlib.GameProfile");
                gameProfileExists = true;
            } catch (ClassNotFoundException ignored) {
            
            }
            try {
                Class.forName("com.mojang.authlib.GameProfile");
                gameProfileExists = true;
            } catch (ClassNotFoundException ignored) {
            
            }
            if (gameProfileExists) {
                profile = (GameProfile)entityPlayer.getClass().getMethod("getProfile").invoke(entityPlayer);
            }
        }catch(Exception e) {
            
            return;
        }
        try {
            HttpsURLConnection connection = (HttpsURLConnection) new URL(String.format("https://sessionserver.mojang.com/session/minecraft/profile/%s",
                    UUIDTypeAdapter.fromUUID(uuid))).openConnection();
            if (connection!=null && connection.getResponseCode() == HttpsURLConnection.HTTP_OK) {
                String reply = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
                String skin = reply.split("\"value\":\"")[1].split("\"")[0];
                String signature = reply.split("\"signature\":\"")[1].split("\"")[0];
                profile.getProperties().put("textures", new Property("textures", skin, signature));
            } else {
                System.out.println("Connection could not be opened (Response code " + connection.getResponseCode() + ", " + connection.getResponseMessage() + ")");
            }
        } catch (Exception e) {
        }
    }
    
}
