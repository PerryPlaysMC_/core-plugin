package me.perryplaysmc.plugin.commands.util.chat;

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Info;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.ListsHelper;

import java.util.Arrays;
import java.util.List;

public class CommandChat extends Command {
    
    public CommandChat() {
        super("chatadmin", "ca", "chat", "chat-admin");
        addDefaultPermission("chat.clear.*", "chat.*");
        setDescription("Commands for the Chat");
    }
    
    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        if(args.length == 0) {
            mustBe(true);
            sendMessage("Commands.Chat.user-Chat-Toggle", s.getUser().getChatSettings().isEnabled() ? "disabled" : "enabled");
            s.getUser().getChatSettings().toggle();
            return;
        }
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("clear")) {
                hasPermission("chat.clear.all");
                for(int i = 0; i < 100; i++)
                    CoreAPI.broadcast("");
                CoreAPI.broadcast(CoreAPI.format("Commands.Chat.Clear.All", (s.isVanished() ? "an Admin" : s.getName())));
                
            }else if(args[0].equalsIgnoreCase("toggle")) {
                hasPermission("chat.toggle");
                boolean isMuted = CoreAPI.getSettings().getBoolean("settings.Chat.muted");
                CoreAPI.getChat().set("settings.muted", !isMuted);
                sendMessage("Commands.Chat.toggle", isMuted ? "unMuted" : "Muted", !isMuted);
            }else if(args[0].equalsIgnoreCase("toggleLinks")) {
                mustBe(true);
                hasPermission("chat.toggle.links");
                s.getUser().getChatSettings().toggleLinks();
                sendMessage("Commands.Chat.user-Chat-Toggle-Links", s.getUser().getChatSettings().allowLinks() ? "enabled" : "disabled", !s.getUser().getChatSettings().allowLinks());
            }else sendHelp();
            return;
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("clear")) {
                if(args[1].equalsIgnoreCase("self")) {
                    hasPermission("chat.clear.self");
                    for(int i = 0; i < 100; i++)
                        s.sendMessage("");
                    s.sendMessage(CoreAPI.getMessages().getString("Commands.Chat.Clear.send-Self"));
                }else {
                    hasPermission("chat.clear.other");
                    User u = getUser(args[1]);
                    for(int i = 0; i < 100; i++)
                        u.sendMessage("");
                    s.sendMessageFormat("Commands.Chat.Clear.Targeted.Sender", u.getName());
                    u.sendMessageFormat("Commands.Chat.Clear.Targeted.User", (s.isVanished() ? "an Admin" : s.getName()));
                }
            }else if(args[0].equalsIgnoreCase("slow")) {
                if(args[1].equalsIgnoreCase("toggle")) {
                    hasPermission("chat.slow.toggle");
                    boolean isSlow = CoreAPI.getChat().getBoolean("settings.Slow.enabled");
                    CoreAPI.getChat().set("settings.Slow.enabled", !isSlow);
                    sendMessage("Commands.Chat.Slow.toggle", isSlow ? "disabled" : "enabled", !isSlow);
                }else sendHelp();
            }else sendHelp();
            return;
        }
        if(args.length == 3) {
            if(args[0].equalsIgnoreCase("slow")) {
                if(args[1].equalsIgnoreCase("settime")) {
                    hasPermission("chat.slow.settime");
                    double i = getDouble(args[2]);
                    CoreAPI.getChat().set("settings.Slow.time", i);
                    sendMessage("Commands.Chat.Slow.set-Time", i);
                }
            }
            return;
        }
        sendHelp();
    }
    
    
    void sendHelp() {
        Info info = new Info(this);
        
        info.addSub(new String[] {"chat.clear", "chat.clear.other"},"clear [o][self/target]")
                .tooltip("[c]Clear the chat for your self or another player or globally!", "[c]Click to insert: /[pc]chat [r]clear [a]<self/target>")
                .suggest("/chat [r]clear [a]<self/target>");
        
        info.addSub(new String[] {"chat.slow.toggle"}, "toggle")
                .tooltip("[c]Toggle the chat (Mute/UnMute)", "[c]Click to insert: /[pc]chat [r]toggle")
                .suggest("/chat toggle");
    
        info.addSub(new String[] {"chat.slow.toggle.links"}, "togglelinks")
                .tooltip("[c]Toggle the chat links (Allow clickable links)", "[c]Click to insert: /[pc]chat [r]togglelinks")
                .suggest("/chat togglelinks");
        
        info.addSub(new String[] {"chat.slow.toggle"},"slow [o]toggle")
                .tooltip("[c]Toggle the chat SlowMode (Enabled/Disable)", "[c]Click to insert: /[pc]chat [r]slow [o]toggle")
                .suggest("/chat slow toggle");
    
        info.addSub(new String[] {"chat.slow.settime"},"slow [o]settime [a]<time>")
                .tooltip("[c]Set the chat SlowMode Time (Delay in chat)", "[c]Click to insert: /[pc]chat [r]slow [o]settime [a]<time>")
                .suggest("/chat slow settime <time>");
        info.send(s);
    }
    
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) {
        List<String> f = Lists.newArrayList();
        f.addAll(loadTab(1, Arrays.asList("clear", "slow", "toggle", "toggleLinks")));
        f.addAll(loadTab(2, "clear", ListsHelper.createList(CoreAPI.getUserNames()).add("self")));
        f.addAll(loadTab(2, "slow", Arrays.asList("settime", "toggle")));
        return f;
    }
}
