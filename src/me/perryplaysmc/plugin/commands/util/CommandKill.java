package me.perryplaysmc.plugin.commands.util;
/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 7/31/19-2023
 **/

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Info;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.ListsHelper;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandKill extends Command {
    
    public CommandKill() {
        super("kill");
        setDescription("Kill entities with ease!");
        addDefaultPermission("kill.*");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(args.length == 1) {
            World w = s.isPlayer() ? s.getUser().getWorld() : Bukkit.getWorlds().get(0);
            HashMap<EntityType, Integer> mobs = new HashMap<>();
            if(args[0].equalsIgnoreCase("all")) {
                hasPermission("kill.all");
                for (Entity e : w.getEntities()) {
                    if(e.getType() != EntityType.PLAYER) {
                        e.remove();
                        mobs.put(e.getType(), mobs.containsKey(e.getType()) ? mobs.get(e.getType()) + 1 : 1);
                    }
                }
                Message msg = new Message("[c]Entities killed &o&7[Hover for more info!]");
                String m = "";
                for (Map.Entry<EntityType, Integer> entities : mobs.entrySet()) {
                    m += m.isEmpty() ? "[pc]" + StringUtils.getNameFromEnum(entities.getKey()) + ": [c]" + entities.getValue()
                            : "\n[pc]" + StringUtils.getNameFromEnum(entities.getKey()) + "[c]: " + entities.getValue();
                }
                msg.tooltip(m.isEmpty() ? "[c]No entities found" : m);
                msg.send(s);
                return;
            }else sendHelp();
            return;
        }
        if(args.length == 2) {
            World w = s.isPlayer() ? s.getUser().getWorld() : Bukkit.getWorlds().get(0);
            if(args[0].equalsIgnoreCase("player")) {
                hasPermission("kill.player");
                if(args[1].equalsIgnoreCase("*")) {
                    hasPermission("kill.player.all");
                    for(User u : CoreAPI.getOnlineUsers())
                        if(s.isPlayer() && !u.getUniqueId().toString().equalsIgnoreCase(s.getUser().getUniqueId().toString()))
                            u.kill();
                        else if(!s.isPlayer())
                            u.kill();
                            sendMessage("Commands.Kill.player-all");
                    return;
                }
                User u = getUser(args[1]);
                u.kill();
                sendMessage("Commands.Kill.player", u.getName());
            }else if(args[0].equalsIgnoreCase("entity")) {
                hasPermission("kill.entity");
                EntityType u = getEntityType(args[1]);
                for(Entity e : w.getEntities()) {
                    if(e.getType() == u) e.remove();
                }
                sendMessage("Commands.Kill.entity", StringUtils.getNameFromEnum(u));
            }else sendHelp();
            return;
        }
        sendHelp();
    }
    
    void sendHelp() {
        Info info = new Info(this);
        info.addSub(new String[]{"kill.all"}, "all").tooltip("[c]Kills all entities", "[c]Click to run /[pc]kill [r]all").command("/kill all");
        
        info.addSub(new String[]{"kill.player"}, "player [a]<player>")
                .tooltip("[c]Kill specified Player", "[c]Click to insert /[pc]kill [r]player [a]<player>")
                .suggest("/kill player <player>");
        
        info.addSub(new String[]{"kill.entity"}, "entity [a]<entity>")
                .tooltip("[c]Kills all entities of the specified type", "[c]Click to insert /[pc]kill [r]entity [a]<entity>")
                .suggest("/kill entity <entity>");
    }
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) {
        List<String> f = Lists.newArrayList();
        f.addAll(loadTab(1, Arrays.asList("player", "entity", "all")));
        f.addAll(loadTab(2, "player", ListsHelper.createList(CoreAPI.getUserNames()).add("*")));
        f.addAll(loadTab(2, "entity", CoreAPI.findNames(EntityType.values())));
        return f;
    }
}
