package me.perryplaysmc.plugin.commands.util.homes;
/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/5/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.handlers.Home;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.string.ListsHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("all")
public class CommandSetHome extends Command {
    
    UUID arg;
    private HashMap<CommandSource, String> h = new HashMap<>();
    
    public CommandSetHome() {
        super("setHome", "homeset");
        arg = UUID.randomUUID();
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        mustBe(true);
        hasPermission("sethome");
        if(args.length == 0) {
            sendHelp();
            return;
        } else if(args.length == 1) {
            if(args[0].contains(":", "?", ">", "<", "@", "'")) {
                String x = args[0].getContains();
                sendMessage("Commands.Home.Set.cannot-Contain-Character", args[0].getContents(), x);
                return;
            }
            String highestGroup="";
            List<Home> homes = s.getUser().getHomeData().getHomes();
            if(CoreAPI.getSettings().getBoolean("HomeSettings.maxHomes")&&!s.getBase().isOp()&&!s.hasPermission("homes.unlimited")) {
                String group = "";
                List<String> groups  = new ArrayList<>();
                for(String a : CoreAPI.getSettings().getSection("HomeSettings.groups").getKeys(false)) groups.add(a);
                int highest = 0;
                for(int i = 1; i < groups.size(); i++) {
                    if(s.hasPermission("homes."+groups.get(i)) &&
                       (CoreAPI.getSettings().getInt("HomeSettings.groups."+groups.get(i)))
                       < (CoreAPI.getSettings().getInt("HomeSettings.groups."+groups.get(highest)))) highest = i;
                }
                highestGroup = groups.get(highest);
                if(s.hasPermission("homes." + highestGroup)) {
                    group = highestGroup;
                }
                if(highestGroup == "") {
                    if(homes.size() >= CoreAPI.getSettings().getInt("HomeSettings.default")) {
                        sendMessage("Home.Set.maxHomes");
                        return;
                    }
                }else {
                    if(homes.size() >= CoreAPI.getSettings().getInt("HomeSettings.groups."+highestGroup)) {
                        sendMessage("Home.Set.maxHomes");
                        return;
                    }
                }
            }
            if(s.getUser().getHomeData().getHome(args[0].getContents()) != null && CoreAPI.getMessages().getBoolean("Commands.Home.Set.Override.doCheck")) {
                Message msg = new Message(CoreAPI.format("Commands.Home.Set.Override.message"));
                msg.then("\n");
                msg.then(CoreAPI.format("Commands.Home.Set.Override.confirm")).command("/sethome confirm " + arg.toString());
                msg.then(" ");
                msg.then(CoreAPI.format("Commands.Home.Set.Override.cancel")).command("/sethome cancel " + arg.toString());
                msg.send(s);
                h.put(s, args[0].getContents());
                return;
            }
            s.getUser().getHomeData().setHome(args[0].getContents());
            sendMessage("Commands.Home.Set.set", args[0].getContents());
        }else if(args.length == 2) {
            if(!h.containsKey(s)||!args[1].equalsIgnoreCase(arg.toString())) {
                sendMessage("Commands.Home.Set.Override.error");
                return;
            }
            if(args[1].equalsIgnoreCase(arg.toString())) {
                if(args[0].equalsIgnoreCase("confirm")) {
                    s.getUser().getHomeData().setHome(h.get(s));
                    sendMessage("Commands.Home.Set.Override.overridden", h.get(s));
                    h.remove(s);
                }else if(args[0].equalsIgnoreCase("cancel")) {
                    sendMessage("Commands.Home.Set.Override.cancelled", h.get(s));
                    h.remove(s);
                }
            }else
                sendMessage("Commands.Home.Set.Override.error");
        }else sendHelp();
    }
    
    void sendHelp() {
        Message msg = new Message("[c]/[pc]sethome [a]<home>").suggest("/sethome ");
        msg.send(s);
    }
    
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        List<String> f = Lists.newArrayList();
        mustBe(true);
        f.addAll(loadTab(1, ListsHelper.createList(CoreAPI.findNames(s.getUser().getHomeData().getHomes()))));
        return f;
    }
}
