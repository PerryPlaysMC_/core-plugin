package me.perryplaysmc.plugin.commands.util.homes;
/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/5/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.handlers.Home;
import me.perryplaysmc.utils.string.ListsHelper;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("all")
public class CommandDelHome extends Command {
    
    UUID arg;
    private HashMap<CommandSource, String> h = new HashMap<>();
    public CommandDelHome() {
        super("DelHome");
        arg = UUID.randomUUID();
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        mustBe(true);
        hasPermission("delhome");
        if(args.length==0){
            sendHomes(s.getUser());
        }else
        if(args.length == 1) {
            Home del = getHome(s.getUser(), args[0]);
            if(s.getUser().getHomeData().getHome(args[0].getContents()) != null && CoreAPI.getMessages().getBoolean("Commands.Home.Delete.Confirm.doCheck")) {
                Message msg = new Message(CoreAPI.format("Commands.Home.Delete.Confirm.message"));
                msg.then("\n");
                msg.then(CoreAPI.format("Commands.Home.Delete.Confirm.confirm")).command("/delhome confirm " + arg.toString());
                msg.then(" ");
                msg.then(CoreAPI.format("Commands.Home.Delete.Confirm.cancel")).command("/delhome cancel " + arg.toString());
                msg.send(s);
                h.put(s, args[0].getContents());
                return;
            }
            s.getUser().getHomeData().delHome(del.getName());
            sendMessage("Commands.Home.Delete.delete", args[0].getContents());
        }else
        if(args.length == 2) {
            if(!h.containsKey(s)||!args[1].equalsIgnoreCase(arg.toString())) {
                sendMessage("Commands.Home.Delete.Confirm.error");
                return;
            }
            if(args[1].equalsIgnoreCase(arg.toString())) {
                if(args[0].equalsIgnoreCase("confirm")) {
                    s.getUser().getHomeData().delHome(h.get(s));
                    sendMessage("Commands.Home.Delete.Confirm.overridden", h.get(s));
                    h.remove(s);
                }else if(args[0].equalsIgnoreCase("cancel")) {
                    sendMessage("Commands.Home.Delete.Confirm.cancelled", h.get(s));
                    h.remove(s);
                }
            }else
                sendMessage("Commands.Home.Delete.Confirm.error");
        }else sendHelp();
    }
    
    void sendHelp() {
        Message msg = new Message("[c]/[pc]delhome [a]<home>").suggest("/delhome ");
        msg.send(s);
    }
    
    
    void sendHomes(OfflineUser u) {
        List<Home> homes = u.getHomeData().getHomes();
        Message msg = new Message(CoreAPI.format("Commands.Home.Delete.List.Header", u.getRealName()) + "\n");
        
        String e = CoreAPI.getMessages().getString("Commands.Home.Delete.List.format");
        for(int i = 0; i < homes.size(); i++) {
            String a = e.split("\\{0}")[1];
            if(i == homes.size() && a.endsWith(",")) {
                a = a.substring(0, a.lastIndexOf(',')) + "." + a.substring(a.lastIndexOf(',')+1);
            }
            Home home = homes.get(i);
            msg.then(e.split("\\{0}")[0] + home.getName());
            msg.tooltip(CoreAPI.formatArray("Commands.Home.Delete.List.hover", home.getName(), home.getLocation().getBlockX()
                    , home.getLocation().getBlockY(), home.getLocation().getBlockZ()
            ));
            if(CoreAPI.getMessages().getBoolean("Commands.Home.Delete.List.runCommand")) {
                msg.command(CoreAPI.format("Commands.Home.Delete.List.command", home.getName()));
            }else {
                msg.suggest(CoreAPI.format("Commands.Home.Delete.List.command", home.getName()));
            }
        }
        if(homes.size() == 0)
            msg.then(CoreAPI.format("Commands.Home.Delete.List.not-Set"));
        
        String c = "";
        for(char a : u.getRealName().toCharArray())
            c+="-";
        
        msg.then("\n" + CoreAPI.format("Commands.Home.Delete.List.Footer", c, u.getRealName()));
        msg.send(s);
    }
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        List<String> f = Lists.newArrayList();
        mustBe(true);
        f.addAll(loadTab(1, ListsHelper.createList(CoreAPI.findNames(s.getUser().getHomeData().getHomes()))));
        return f;
    }
    
}
