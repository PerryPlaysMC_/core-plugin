package me.perryplaysmc.plugin.commands.util.homes;
/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/5/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.handlers.Home;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.utils.string.ListsHelper;

import java.util.List;

@SuppressWarnings("all")
public class CommandHome extends Command {
    
    public CommandHome() {
        super("home", "homes", "h");
    }
    
    public void run(CommandSource s, CommandLabel cl, Argument[] args) throws CommandException {
        hasPermission("home");
        if(cl.equalsIgnoreCase("homes")) {
            if(args.length == 0) {
                mustBe(true);
                sendHomes(s.getUser());
            }
            else if(args.length == 1) {
                OfflineUser u = getAnyUser(args[0]);
                if(u.getRealName().equalsIgnoreCase(s.getRealName())) {
                    sendHomes(s.getUser());
                    return;
                }
                hasPermission("home.other");
                sendHomes(u);
            }else {
                sendUnknown();
            }
            return;
        }
        if(args.length == 0){
            sendHomes(s.getUser());
        }
        if(args.length == 1) {
            mustBe(true);
            if(args[0].contains(":")) {
                hasPermission("home.other");
                OfflineUser u = getAnyUser(args[0].split(":")[0]);
                if(args[0].split(":").length==1) {
                    sendHomes(u);
                    return;
                }
                Home home = getHome(u, args[0].split(":")[1]);
                s.getUser().teleport(home.getLocation());
                sendMessage("Commands.Home.Home.home-other", u.getRealName(), home.getName());
                return;
            }
            Home home = getHome(s.getUser(), args[0]);
            s.getUser().teleport(home.getLocation());
            sendMessage("Commands.Home.Home.home", home.getName());
        }
    }
    
    
    void sendHomes(OfflineUser u) {
        List<Home> homes = u.getHomeData().getHomes();
        Message msg = new Message(CoreAPI.format("Commands.Home.List.Header", u.getRealName())+"\n");
        
        String e = CoreAPI.getMessages().getString("Commands.Home.List.format");
        for(int i = 0; i < homes.size(); i++) {
            String a = e.split("\\{0}")[1];
            if(i == homes.size() && a.endsWith(",")) {
                a = a.substring(0, a.lastIndexOf(',')) + "." + a.substring(a.lastIndexOf(',')+1);
            }
            Home home = homes.get(i);
            msg.then(e.split("\\{0}")[0] + home.getName());
            msg.tooltip(CoreAPI.formatArray("Commands.Home.List.hover",
                    home.getName(),
                    home.getLocation().getBlockX()
                    , home.getLocation().getBlockY(),
                    home.getLocation().getBlockZ()
            ));
            if(CoreAPI.getMessages().getBoolean("Commands.Home.List.runCommand")) {
                msg.command(CoreAPI.format("Commands.Home.List.command", home.getName()));
            }else {
                msg.suggest(CoreAPI.format("Commands.Home.List.command", home.getName()));
            }
            msg.then(a);
        }
        if(homes.size() == 0)
            msg.then(CoreAPI.format("Commands.Home.List.not-Set"));
        
        String c = "";
        for(char a : u.getRealName().toCharArray())
            c+="-";
        
        msg.then("\n" + CoreAPI.format("Commands.Home.List.Footer", c, u.getRealName()));
        msg.send(s);
    }
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        List<String> f = Lists.newArrayList();
        mustBe(true);
        f.addAll(loadTab(1, CoreAPI.findNames(s.getUser().getHomeData().getHomes())));
        f.addAll(loadTab(1, args[0].length()>2&&args[0].startsWith(CoreAPI.getAllUserNames()), ListsHelper.createList(CoreAPI.getAllUserNames())));
        if(args[0].contains(":")) {
            OfflineUser u = getAnyUser(args[0].split(":")[0]);
            for(Home h : u.getHomeData().getHomes()) {
                if(args[0].split(":")[0].toLowerCase().startsWith(h.getName().toLowerCase())) f.add(u.getName()+":"+h.getName().toLowerCase());
            }
            return f;
        }
        return f;
    }
    
}
