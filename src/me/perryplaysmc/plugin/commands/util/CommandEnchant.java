package me.perryplaysmc.plugin.commands.util;

import com.google.common.collect.Lists;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.enchantment.CustomEnchant;
import me.perryplaysmc.enchantment.EnchantHandler;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.string.ListsHelper;
import me.perryplaysmc.utils.inventory.ingorish.EnchantUtil;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.NumberUtil;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class CommandEnchant extends Command {
    public CommandEnchant() {
        super("enchant");
        setDescription("Enchant the item in your hand.");
    }
    
    
    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        mustBe(true);
        hasPermission("enchant");
        if(args.length == 0) {
            argsLengthError(false);
            return;
        }
        if(args.length == 1) {
            ItemStack i = s.getUser().getItemInHand();
            if(i == null || i.getType() == Material.AIR) {
                sendMessage("Commands.invalid-Item");
                return;
            }
            Enchantment x = getEnchantment(args[0]);
            ItemMeta im = i.getItemMeta();
            if(x instanceof CustomEnchant) {
                CustomEnchant c = (CustomEnchant) x;
                im = EnchantUtil.addEnchantment(i, c, c.getMaxLevel());
                i.setItemMeta(im);
            }else {
                if(im != null && im.hasEnchant(x)) {
                    im.removeEnchant(x);
                    i.setItemMeta(im);
                }
                im = EnchantUtil.addEnchantment(i, x, x.getMaxLevel());
            }
            i.setItemMeta(im);
            sendMessage("Commands.Enchant.success", EnchantUtil.fixName(x), x.getMaxLevel(), NumberUtil.convertToRoman(x.getMaxLevel()));
        }
        if(args.length == 2) {
            ItemStack i = s.getUser().getItemInHand();
            if(i == null || i.getType() == Material.AIR) {
                sendMessage("Commands.invalid-Item");
                return;
            }
            Enchantment x = getEnchantment(args[0]);
            int level = integer(args[1]);
            ItemMeta im = i.getItemMeta();
            if(x instanceof CustomEnchant) {
                CustomEnchant c = (CustomEnchant) x;
                if(level <= 0) {
                    im = EnchantUtil.removeEnchant(i, c);
                    i.setItemMeta(im);
                }
                if(level > 0)
                    im = EnchantUtil.addEnchantment(i, c, level);
                i.setItemMeta(im);
            }else {
                if(level <= 0) {
                    im = EnchantUtil.removeEnchant(i, x);
                    i.setItemMeta(im);
                }
                if(level > 0)
                    im = EnchantUtil.addEnchantment(i, x, level);
            }
            i.setItemMeta(im);
            sendMessage("Commands.Enchant.success", EnchantUtil.fixName(x), level, NumberUtil.convertToRoman(level));
        }
    }
    
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        List<String> f = Lists.newArrayList();
        List<String> enchants = Lists.newArrayList();
        for(Enchantment e : Enchantment.values()) {
            if(!enchants.contains(ColorUtil.removeColor(EnchantUtil.fixName(e).replace(" ", "_").toLowerCase())))
                enchants.add(ColorUtil.removeColor(EnchantUtil.fixName(e).replace(" ", "_").toLowerCase()));
        }
        f.addAll(loadTab(1, ListsHelper.createList(CoreAPI.findNames(EnchantHandler.getEnchantments())).addAll(enchants)));
        if(args.length>1)
            f.addAll(loadTab(2, CoreAPI.createList(getEnchantment(args[0]).getStartLevel(), getEnchantment(args[0]).getMaxLevel())));
        
        return f;
    }
}
