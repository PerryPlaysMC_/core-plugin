package me.perryplaysmc.plugin.commands.util.world;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/4/19-2023
 * Package: me.perryplaysmc.plugin.commands.util.world
 * Path: me.perryplaysmc.plugin.commands.util.world.CommandExperience
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandExperience extends Command {
    
    public CommandExperience() {
        super("experience", "exp", "xp");
        setDescription("give/takes Experience");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(args.length == 2) {
            hasPermission("experience");
            User u = getUser(args[0]);
            boolean isLevel = args[1].getContents().toLowerCase().endsWith("l");
            int exp = integer(isLevel ? args[1].subStringEnd(args[1].length() + -1) : args[1]);
            if(isLevel) {
                if(exp < 0 && (u.getBase().getLevel() +- exp) < 0) {
                    exp = u.getBase().getLevel()+-1;
                    u.getBase().setLevel(1);
                    u.getBase().setExp(0f);
                } else {
                    u.getBase().setLevel(u.getBase().getLevel() + exp);
                }
            } else {
                setExp(u, exp);
            }
            
            if(isLevel) {
                if(exp < 0)
                    sendMessage("Experience.success-Level.Take", u.getRealName(), (exp+"").replace("-", ""));
                else
                    sendMessage("Experience.success-Level.Give", u.getRealName(), exp);
            }else {
                if(exp < 0)
                    sendMessage("Experience.success-Exp.Take", u.getRealName(), (exp+"").replace("-", ""));
                else
                    sendMessage("Experience.success-Exp.Give", u.getRealName(), exp);
            }
        }
    }
    
    private void setExp(User u, int d) {
        double c = d;
        double x = (c/100);
        float p = (float)(c/100)+u.getBase().getExp();
        int lvl = Integer.parseInt((p+"").split("\\.")[0]);
        u.getBase().setExp(Float.parseFloat("0."+(""+p).split("\\.")[1]));
        u.getBase().setLevel(u.getBase().getLevel()+lvl);
        if(d < 0) {
            if((u.getBase().getLevel())<=0) {
                u.getBase().setLevel(0);
                if(p<=0)
                    u.getBase().setExp(0);
            }
            if(p<=0)
                u.getBase().setExp(0);
        }
    }
}
