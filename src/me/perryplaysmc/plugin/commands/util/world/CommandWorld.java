package me.perryplaysmc.plugin.commands.util.world;

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.ConfigManager;
import me.perryplaysmc.world.LocationUtil;
import me.perryplaysmc.world.WorldManager;
import me.perryplaysmc.world.generators.GeneratorCustom;
import me.perryplaysmc.world.generators.GeneratorDefault;
import me.perryplaysmc.world.generators.GeneratorFlatLand;
import me.perryplaysmc.world.generators.GeneratorVoid;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.world.Creator;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CommandWorld extends Command {
    
    public CommandWorld() {
        super("world", "mworld", "mw", "w", "miniworld");
        setDescription("Create/Load/Teleport/Delete worlds!");
    }
    
    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("list")) {
                hasPermission("world.list");
                listWorlds();
            }else if(args[0].equalsIgnoreCase("info")) {
                mustBe(true);
                hasPermission("world.info");
                World w = s.getUser().getBase().getLocation().getWorld();
                sendMessage("Commands.World.Info",
                        w.getName(),
                        w.getSeed(),
                        w.getSeaLevel(),
                        LocationUtil.locationToString(w.getSpawnLocation()),
                        StringUtils.formatList(true, true, Arrays.asList(w.getGameRules())),
                        w.getAllowAnimals(),
                        w.getAllowMonsters(),
                        w.getMonsterSpawnLimit());
            }
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("tp")) {
                mustBe(true);
                hasPermission("world.tp", "world.teleport");
                World w = getWorld(args[1]);
                s.getUser().getBase().teleport(w.getSpawnLocation());
                sendMessage("Commands.World.teleport-To", w.getName());
            }
            else if(args[0].equalsIgnoreCase("listPlayers")) {
                hasPermission("world.listplayers");
                World w = getWorld(args[1]);
                List<String> pl = new ArrayList<>();
                for(Player p : w.getPlayers())
                    pl.add(p.getName());
                if(pl.isEmpty())
                    sendMessage("Commands.World.List.no-Players");
                else
                    sendMessage("Commands.World.List.found", w.getName(), StringUtils.formatList(true, true, pl));
            }else if(args[0].equalsIgnoreCase("create")) {
                hasPermission("world.create");
                worldExists(args[1]);
                World w = Bukkit.createWorld(new Creator(args[1].getContents(), new GeneratorDefault()));
                sendMessage("Commands.World.create", w.getName());
            }else if(args[0].equalsIgnoreCase("delete")) {
                hasPermission("world.delete");
                World w = getWorld(args[1]);
                WorldManager.removeWorld(w);
                CoreAPI.getWorlds().set("Worlds."+w.getName(), null);
                sendMessage("Commands.World.delete", w.getName());
            }else if(args[0].equalsIgnoreCase("load")) {
                hasPermission("world.load");
                File f = ConfigManager.getDirectory(Bukkit.getWorldContainer(), args[1].getContents());
                if(f==null||!f.exists() || !f.isDirectory() || !ConfigManager.containsFile(f, "uid.dat")) {
                    sendMessage("Commands.Error.invalid-World-File", args[1].getContents());
                    return;
                }
                WorldManager.loadWorld(f.getName());
                sendMessage("Commands.World.load", f.getName());
            }
        }
        if(args.length > 2) {
            if(args[0].equalsIgnoreCase("create")) {
                hasPermission("world.create");
                worldExists(args[1]);
                HashMap<String, Object> options = parseOptions(new HashMap<String, Class<? extends Object>>() {{
                    put("generator", String.class);
                }}, 3, args);
                WorldCreator creator = null;
                System.out.println(options.toString());
                if(options.get("generator") != null)
                    switch ((String)options.get("generator")) {
                        case "flatland": {
                            creator = new Creator(args[1].getContents(), new GeneratorFlatLand());
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".generator", "GeneratorFlatLand");
                            break;
                        }
                        case "default": {
                            creator = new Creator(args[1].getContents());
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".generator", "bukkit");
                            creator.type(WorldType.NORMAL);
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.WorldType", "NORMAL");
                            break;
                        }
                        case "void": {
                            creator = new Creator(args[1].getContents(), new GeneratorVoid());
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".generator",  "GeneratorVoid");
                            creator.type(WorldType.FLAT);
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.WorldType", "FLAT");
                            break;
                        }
                        case "custom1": {
                            creator = new Creator(args[1].getContents(), new GeneratorDefault());
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".generator", "GeneratorDefault");
                            creator.type(WorldType.NORMAL);
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.WorldType", "NORMAL");
                            break;
                        }
                        case "custom2": {
                            creator = new Creator(args[1].getContents(), new GeneratorCustom());
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".generator", "GeneratorCustom");
                            creator.type(WorldType.NORMAL);
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.WorldType", "NORMAL");
                            break;
                        }
                        default:
                            creator = new WorldCreator(args[1].getContents());
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".generator",  "bukkit");
                            creator.type(WorldType.NORMAL);
                            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.WorldType", "NORMAL");
                            break;
                    }
    
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.Environment", creator.environment().name());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.GenerateStructures", creator.generateStructures());
                World w = Bukkit.createWorld(creator);
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.MaxHeight", w.getMaxHeight());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.Seed", w.getSeed());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.SeaLevel", w.getSeaLevel());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.Difficulty", w.getDifficulty().name());
                for(String g : w.getGameRules()) {
                    CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.GameRules." + g, w.getGameRuleValue(g));
                }
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.Spawn", w.getSpawnLocation());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.WaterAnimalSpawnLimit", w.getWaterAnimalSpawnLimit());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.MonsterSpawnLimit", w.getMonsterSpawnLimit());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.AmbientSpawnLimit", w.getAmbientSpawnLimit());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.AnimalSpawnLimit", w.getAnimalSpawnLimit());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.AllowAnimals", w.getAllowAnimals());
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.AllowMonsters", w.getAllowMonsters());
                sendMessage("Commands.World.create", w.getName());
            }
        }
    }
    
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) {
        List<String> r = Lists.newArrayList();
        List<String> args1 = Arrays.asList("list", "create", "delete", "tp", "listPlayers", "info", "load");
        List<String> args2 = new ArrayList<>();
        List<String> args3 = Arrays.asList("generator");
        List<String> args4 = Arrays.asList("flatland", "default", "void", "custom1", "custom2");
        for(World w : Bukkit.getWorlds())
            if(!args2.contains(w.getName()))args2.add(w.getName());
        r.addAll(loadTab(1, args1));
        r.addAll(loadTab(2, "tp", args2));
        r.addAll(loadTab(2, "load", args2));
        r.addAll(loadTab(2, "delete", args2));
        r.addAll(loadTab(2, "listPlayers", args2));
        if(args.length > 0 && args[0].equalsIgnoreCase("create")) {
            r.addAll(loadTab(3, args3));
            r.addAll(loadTab(4, "generator", args4));
        }
        return r;
    }
    
    void listWorlds() {
        Message msg = new Message("[c]Worlds: ");
        
        for(int i = 0; i < Bukkit.getWorlds().size(); i++) {
            World w = Bukkit.getWorlds().get(i);
            msg.then("[pc]"+w.getName()).tooltip("[c]Click to teleport to " + w.getName()).command("/world tp " + w.getName());
            
            if(i < Bukkit.getWorlds().size()+-1) {
                msg.then("[c], ");
            }
        }
        msg.send(s);
        
        
    }
    
}
