package me.perryplaysmc.plugin.commands.util.world;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import org.bukkit.World;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/10/19-2023
 * Package: me.perryplaysmc.plugin.commands.util.world
 * Class: CommandSpawn
 * <p>
 * Path: me.perryplaysmc.plugin.commands.util.world.CommandSpawn
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandSpawn extends Command {
    
    public CommandSpawn() {
        super("spawn");
        setDescription("teleports the player to the world spawn");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        hasPermission("spawn");
        if(args.length == 0) {
            if(!s.isPlayer()) {
                specifyUser();
                return;
            }
            if(!CoreAPI.getLocations().isSet("Spawns." + s.getUser().getWorld().getName())) {
                sendMessage("Spawn.fail", s.getUser().getWorld().getName());
                return;
            }
            s.getUser().teleport(CoreAPI.getLocations().getLocation("Spawns." + s.getUser().getWorld().getName()));
            sendMessage("Spawn.success", s.getUser().getWorld().getName());
            return;
        }
        if(args.length == 1) {
            hasPermission("spawn.other");
            User t = getAllUser(args[0]);
            if(t.getName()==s.getName()) {
                run(s, commandLabel, new Argument[0]);
                return;
            }
            World w = s.isPlayer() ? s.getUser().getWorld() : t.getWorld();
            if(!CoreAPI.getLocations().isSet("Spawns." + w.getName())) {
                sendMessage("Spawn.fail", w.getName());
                return;
            }
            t.teleport(CoreAPI.getLocations().getLocation("Spawns." + w.getName()));
            sendMessage("Spawn.success", w.getName());
            return;
        }
        argsLengthError(true);
    }
    
}
