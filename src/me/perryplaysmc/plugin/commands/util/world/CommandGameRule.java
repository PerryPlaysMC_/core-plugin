package me.perryplaysmc.plugin.commands.util.world;

import me.perryplaysmc.Main_Core;
import me.perryplaysmc.command.*;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandGameRule extends Command {
    public CommandGameRule() {
        super("gamerule");
    }
    
    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        hasPermission("gamerule");
        if(args.length == 1) {
            GameRule<?> gr = getGameRule(args[0]);
            sendMessage("Commands.GameRule.send-Value", gr.getName(), s.isPlayer() ? s.getUser().getWorld().getGameRuleValue(gr.getName())
                    : Bukkit.getWorlds().get(0).getGameRuleValue(gr.getName()));
            return;
        }
        if(args.length == 2) {
            me.perryplaysmc.command.GameRule gr = getGameRule(args[0]);
            World w =  s.isPlayer() ? s.getUser().getWorld() : Bukkit.getWorlds().get(0);
            String value = args[1].getContents();
            if(args[1].equalsIgnoreCase("default")) {
                value = gr.getDefaultValue().toString();
            }
            w.setGameRuleValue(gr.getName(), value);
            Main_Core.getPlugin(Main_Core.class).setWorld(w);
            sendMessage("Commands.GameRule.send-Set", gr.getName(), s.isPlayer() ? s.getUser().getWorld().getGameRuleValue(gr.getName()) :
                    Bukkit.getWorlds().get(0).getGameRuleValue(gr.getName()));
            return;
        }argsLengthError(true);
    }
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) {
    
        List<String> arg1 = Arrays.asList(s.getUser().getBase().getWorld().getGameRules());
        List<String> f = new ArrayList<>(loadTab(1, arg1));
        if(args.length == 2) {
            GameRule<?> g = getGameRule(args[0].getContents());
            if(g!=null) {
                f.addAll(loadTab(2, g.getName(), g.getType() == Boolean.class ? Arrays.asList("true", "false", "default") : Arrays.asList("default")));
                if((!args[1].equalsIgnoreCase("default")&&!args[1].startsWith("default"))&&g.getType()!=Boolean.class)
                    s.sendAction("&cGameRule type >> " + g.getType().getSimpleName());
            } else s.sendTitle("&cInvalid Gamerule");
        }
        return f;
    }
    
    GameRule getGameRule(String name) {
        for(GameRule g : GameRule.values())
            if(g.getName().equalsIgnoreCase(name))
                return g;
        return null;
    }
}
