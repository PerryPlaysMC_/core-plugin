package me.perryplaysmc.plugin.commands.util.world;
/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 7/31/19-2023
 **/

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Info;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.ListsHelper;
import me.perryplaysmc.user.CommandSource;
import org.bukkit.Location;

import java.text.DecimalFormat;
import java.util.List;

public class CommandTeleport extends Command {
    
    public CommandTeleport() {
        super("teleport", "tp");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(args.length == 1) {
            hasPermission("teleport.self");
            mustBe(true);
            User u = s.getUser();
            User t = getUser(args[0]);
            if(t.getName().equalsIgnoreCase(s.getName())) {
                sendMessage("Commands.Teleport.Error");
                return;
            }
            u.teleport(t.getLocation());
            sendMessage("Commands.Teleport.teleport-Sender", t.getName());
            if(getBoolean("Commands.Teleport.informTarget"))
                sendMessage(t,"Commands.Teleport.teleport-Target", s.getName());
            return;
        }else if(args.length == 2) {
            User t1 = getUser(args[0]), t2 = getUser(args[1]);
            if(t1.getName().equalsIgnoreCase(s.getName())) {
                run(s, commandLabel, new Argument[] {args[1]});
                return;
            }
            hasPermission("teleport.other");
            t1.teleport(t2.getLocation());
            if(getBoolean("Commands.Teleport.teleport.informTarget-1"))
                sendMessage(t1, "Commands.Teleport.teleport.Send-Target-1", s.getName(), t2.getName());
            if(!t2.getName().equalsIgnoreCase(s.getName()))
                if(getBoolean("Commands.Teleport.teleport.informTarget-2"))
                    sendMessage(t2, "Commands.Teleport.teleport.Send-Target-2", s.getName(), t1.getName());
            sendMessage("Commands.Teleport.teleport.Send-Sender", t1.getName(), t2.getName(), s.getName());
            return;
        }
        else if(args.length == 3) {
            mustBe(true);
            hasPermission("teleport.self.xyz");
            Location newLoc = getLocation(s.getUser(), args[0], args[1], args[2]);
            User u = s.getUser();
            u.teleport(newLoc);
            DecimalFormat f = new DecimalFormat("###.###");
            sendMessage("Commands.Teleport.teleport-XYZ", f.format(newLoc.getX()), f.format(newLoc.getY()), f.format(newLoc.getZ()));
            return;
        }
        else if(args.length == 4) {
            hasPermission("teleport.other.xyz");
            User t = getUser(args[0]);
            if(t.getName().equalsIgnoreCase(s.getName())) {
                run(s, commandLabel, new Argument[] {args[1], args[2], args[3]});
                return;
            }
            Location newLoc = getLocation(t, args[1], args[2], args[3]);
            t.teleport(newLoc);
            DecimalFormat f = new DecimalFormat("###.###");
            sendMessage("Commands.Teleport.teleport-XYZ-other.Sender", t.getName(), f.format(newLoc.getX()), f.format(newLoc.getY()), f.format(newLoc.getZ()));
            if(getBoolean("Commands.Teleport.teleport-XYZ-other.informTarget"))
                sendMessage(t,"Commands.Teleport.teleport-XYZ-other.Target", s.getName(), f.format(newLoc.getX()), f.format(newLoc.getY()), f.format(newLoc.getZ()));
            return;
        }
        sendHelp();
    }
    
    void sendHelp() {
        Info info = new Info(this);
        info.addSub(new String[]{"teleport.xyz"}, "(~ ~ ~)")
                .tooltip("[c]Teleport to Location", "[c]Click to insert /[pc]teleport [r]~ ~ ~")
                .suggest("/teleport ~ ~ ~");
        info.addSub(new String[]{"teleport.xyz"}, "[a]<user> [r](~ ~ ~)")
                .tooltip("[c]Teleport a User to Location", "[c]Click to insert /[pc]teleport [a]<user> [r]~ ~ ~")
                .suggest("/teleport <user> ~ ~ ~");
        
    }
    
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) {
        List<String> f = Lists.newArrayList();
        f.addAll(loadTab(1, CoreAPI.getUserNames()));
        f.addAll(loadTab(2, CoreAPI.getUser(args[0].getContents()) != null, ListsHelper.createList("~", "~ ~", "~ ~ ~").addAll(CoreAPI.getUserNames())));
        f.addAll(loadTab(1, ListsHelper.createList("~", "~ ~", "~ ~ ~")));
        f.addAll(loadTab(2, ListsHelper.createList("~", "~ ~")));
        f.addAll(loadTab(3, ListsHelper.createList("~")));
        f.addAll(loadTab(4, CoreAPI.getUser(args[0].getContents()) != null, ListsHelper.createList("~")));
        
        return f;
    }
    
    Location getLocation(User u, Argument arg1, Argument arg2, Argument arg3) throws CommandException {
        double x, y, z;
        if(arg1.startsWith("~"))
            x = u.getLocation().getX() + getDouble(arg1.equalsIgnoreCase("~") ? "0" : arg1.subString(1));
        else x = getDouble(arg1);
        if(arg2.startsWith("~"))
            y = u.getLocation().getY() + getDouble(arg2.equalsIgnoreCase("~") ? "0" : arg2.subString(1));
        else y = getDouble(arg2);
        if(arg3.startsWith("~"))
            z = u.getLocation().getZ() + getDouble(arg3.equalsIgnoreCase("~") ? "0" : arg3.subString(1));
        else z = getDouble(arg3);
        return new Location(u.getWorld(), x, y, z, u.getLocation().getYaw(), u.getLocation().getPitch());
    }
    
}
