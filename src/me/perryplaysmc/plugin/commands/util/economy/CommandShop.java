package me.perryplaysmc.plugin.commands.util.economy;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.listener.ShopAddItemsEvents;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.inventory.shops.GUIShopHandler;
import me.perryplaysmc.utils.inventory.shops.events.ShopInteract;

public class CommandShop extends Command {
    public CommandShop() {
        super("shop");
    }

    @Override
    public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("edit")) {
                mustBe(true);
                hasPermission("shop.additem");
                ShopAddItemsEvents.load();
                ShopAddItemsEvents.Show(s.getUser());
            }else if(args[0].equalsIgnoreCase("reload")) {
                hasPermission("shop.reload");
                s.sendMessage("[c]Reloaded shops");
                GUIShopHandler.reload();
            }else {
                s.sendMessage("[c]Correct usage: /shop reload");
                s.sendMessage("[c]Correct usage: /shop item add <itemname/hand> <sellprice> <buyprice>");
            }
        }else {
            ShopInteract.load();
            ShopInteract.showInventory(s.getUser());
        }
    }
}
