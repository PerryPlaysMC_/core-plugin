package me.perryplaysmc.plugin.commands.util.economy;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/11/19-2023
 * Package: me.perryplaysmc.sparrow.plugin.commands.util.economy
 * Path: CommandBalance
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandBalance extends Command {
    
    public CommandBalance() {
        super("balance", "bal", "money");
        setDescription("Check users Balance");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(args.length == 0) {
            mustBe(true);
            hasPermission("eco.balance");
            sendMessage("Commands.Balance.self", s.getUser().getAccount().getBalance());
        }
        if(args.length == 1) {
            hasPermission("eco.balance.other");
            OfflineUser t = getAnyUser(args[0]);
            if(t.getName().equalsIgnoreCase(s.getName()))
                run(s, commandLabel, new Argument[0]);
            sendMessage("Commands.Balance.other", t.getName(), t.getAccount().getBalance());
        }
    }
    
}
