package me.perryplaysmc.plugin.commands.util.economy;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.string.StringUtils;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/11/19-2023
 * Package: me.perryplaysmc.sparrow.plugin.commands.util.economy
 * Path: CommandBalanceTop
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandBalanceTop extends Command {
    
    public CommandBalanceTop() {
        super("BalanceTop", "baltop");
        setDescription("get the top Balances");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        hasPermission("economy.balancetop");
        if(args.length==0) {
            s.sendMessage(StringUtils.getBalanceTops(1));
        }
        else if(args.length==1) {
            s.sendMessage(StringUtils.getBalanceTops(integer(args[0])));
        }else
            argsLengthError(true);
    }
    
}
