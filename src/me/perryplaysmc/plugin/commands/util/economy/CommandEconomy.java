package me.perryplaysmc.plugin.commands.util.economy;

import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Info;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.utils.string.ListsHelper;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.utils.string.StringUtils;

import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/5/19-2023
 * Package: me.perryplaysmc.sparrow.plugin.commands.util.economy
 * Path: CommandEconomy
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandEconomy extends Command {
    
    public CommandEconomy() {
        super("economy", "eco");
        setDescription("Allows you to Edit user balances");
        addDefaultPermission("economy.*");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(args.length == 0 ){
            sendHelp();
            return;
        }
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("top", "baltop", "balancetop")) {
                s.sendMessage(StringUtils.getBalanceTops(1));
                return;
            }
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("top", "baltop", "balancetop")) {
                s.sendMessage(StringUtils.getBalanceTops(integer(args[1])));
                return;
            }
        }
        if(args.length != 3) {
            if(args[0].equalsIgnoreCase("add", "give")) {
                hasPermission("economy.give", "economy.add");
                sendHelp(HelpType.GIVE);
                return;
            }
            if(args[0].equalsIgnoreCase("remove", "take")) {
                hasPermission("economy.remove", "economy.take");
                sendHelp(HelpType.TAKE);
                return;
            }
            if(args[0].equalsIgnoreCase("set")) {
                hasPermission("economy.set");
                sendHelp(HelpType.SET);
                return;
            }
            sendHelp();
        }
        if(args.length == 3) {
            OfflineUser t = getAnyUser(args[1]);
            double money = money(args[2]);
            String minBalance = CoreAPI.getSettings().getString("EconomySettings.minBalance");
            String maxBalance = CoreAPI.getSettings().getString("EconomySettings.maxBalance");
            if(args[0].equalsIgnoreCase("add", "give")) {
                hasPermission("economy.give", "economy.add");
                if(money <= 0) {
                    sendMessage("Commands.Economy.error-Money", "give", NumberUtil.format(money));
                    return;
                }
                if(t.getAccount().exceedsMax(false, money)) {
                    sendMessage("Commands.Economy.Give.fail", t.getName(), NumberUtil.format(money), maxBalance);
                    return;
                }
                t.getAccount().addBalance(money);
                sendMessage("Commands.Economy.Give.success", t.getName(), NumberUtil.format(money), t.getAccount().getBalance());
                return;
            }
            if(args[0].equalsIgnoreCase("remove", "take")) {
                hasPermission("economy.remove", "economy.take");
                if(money <= 0) {
                    sendMessage("Commands.Economy.error-Money", "take", NumberUtil.format(money));
                    return;
                }
                if(t.getAccount().exceedsMin(false, money)) {
                    sendMessage("Commands.Economy.Take.fail", t.getName(), NumberUtil.format(money), minBalance);
                    return;
                }
                t.getAccount().removeBalance(money);
                sendMessage("Commands.Economy.Take.success", t.getName(), NumberUtil.format(money), t.getAccount().getBalance());
                return;
            }
            if(args[0].equalsIgnoreCase("set")) {
                hasPermission("economy.remove", "economy.take");
                if(money < 0)
                    if(t.getAccount().exceedsMin(true, money)) {
                        sendMessage("Commands.Economy.Set.fail-Min", t.getName(), NumberUtil.format(money), minBalance);
                        return;
                    }
                if(money >= 0)
                    if(t.getAccount().exceedsMax(true, money)) {
                        sendMessage("Commands.Economy.Set.fail-Max", t.getName(), NumberUtil.format(money), maxBalance);
                        return;
                    }
                t.getAccount().setBalance(money);
                sendMessage("Commands.Economy.Set.success", t.getName(), NumberUtil.format(money), t.getAccount().getBalance());
                return;
            }
        }
        
    }
    
    
    @Override
    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException, IllegalArgumentException {
        List<String> f = Lists.newArrayList();
        f.addAll(loadTab(1, ListsHelper.createList("add", "remove", "set", "top")));
        f.addAll(loadTab(2, CoreAPI.findNamesAsSet(CoreAPI.getAllUsers())));
        f.addAll(loadTab(2, new ArgumentData(0, "top", "baltop", "balancetop"), CoreAPI.createList(1, StringUtils.getBalanceTopsPages()+1)));
        return f;
    }
    
    void sendHelp() {
        Info info = new Info(this);
        info.addSub(new String[] {"economy.give"}, "give [r]<user> [r]<money>")
                .tooltip("[c]Take money from a player",
                        "[c]Arguments:",
                        " [c]- [r]<user>[c] User to give money to",
                        " [c]- [r]<money>[c] The Amount of money to give")
                .suggest("/economy give ");
        info.addSub(new String[] {"economy.take"}, "take [r]<user> [r]<money>")
                .tooltip("[c]Take money from a player",
                        "[c]Arguments:",
                        " [c]- [r]<user>[c] User to take money from",
                        " [c]- [r]<money>[c] The Amount of money to take")
                .suggest("/economy take ");
        info.addSub(new String[] {"economy.set"}, "set [r]<user> [r]<money>")
                .tooltip("[c]Take money from a player",
                        "[c]Arguments:",
                        " [c]- [r]<user>[c] Set balance of user",
                        " [c]- [r]<money>[c] The Amount of money to set balance to")
                .suggest("/economy set ");
        info.send(s);
    }
    
    
    void sendHelp(HelpType type) {
        Message msg = new Message("[c]How to use: ");
        switch (type) {
            
            case GIVE: {
                msg.then("\n[c]/[pc]economy [r]give [r]<user> [r]<money>").suggest("/economy give ");
                break;
            }
            case TAKE: {
                msg.then("\n[c]/[pc]economy [r]take [r]<user> [r]<money>").suggest("/economy take ");
                break;
            }
            case SET: {
                msg.then("\n[c]/[pc]economy [r]set [r]<user> [r]<money>").suggest("/economy set ");
                break;
            }
        }
    }
    
    
    enum HelpType {
        GIVE, TAKE, SET;
    }
    
}
