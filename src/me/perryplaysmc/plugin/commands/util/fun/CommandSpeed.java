package me.perryplaysmc.plugin.commands.util.fun;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/17/19-2023
 * Package: me.perryplaysmc.plugin.commands.util.fun
 * Class: CommandSpeed
 * <p>
 * Path: me.perryplaysmc.plugin.commands.util.fun.CommandSpeed
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandSpeed extends Command {
    
    public CommandSpeed() {
        super("speed");
        setDescription("Sets player speed");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        if(args.length == 1) {
            hasPermission("speed");
            specifyUser();
            User u = s.getUser();
            float speed = speed(integer(args[0]));
            boolean flight = !u.getBase().isOnGround()&& u.getBase().isFlying();
            if(flight)
                u.getBase().setFlySpeed(speed);
            else
                u.getBase().setWalkSpeed(speed);
            sendMessage("Speed.set-" + (flight?"Flight":"Walk"), integer(args[0]));
            return;
        }
        if(args.length == 2) {
            hasPermission("speed.other");
            User u = getUser(args[0]);
            if(u.getName()==s.getName()){
                run(s, commandLabel, new Argument[]{args[1]});
                return;
            }
            float speed = speed(integer(args[1]));
            boolean flight = !u.getBase().isOnGround()&& u.getBase().isFlying();
            if(flight)
                u.getBase().setFlySpeed(speed);
            else
                u.getBase().setWalkSpeed(speed);
            sendMessage("Speed.Target.sender-" + (flight?"Flight":"Walk"), integer(args[1]), u.getName());
            if(!getBoolean("Commands.Speed.Target.informTarget")) return;
            sendMessage(u,"Speed.Target.target-" + (flight?"Flight":"Walk"), integer(args[1]), s.getName());
            return;
        }
        sendUsage();
    }
    
    void sendUsage() throws CommandException {
        if(s.hasPermission("speed"))
            s.sendMessage("[c]/[pc]speed [r]<speed>");
        if(s.hasPermission("speed.other"))
            s.sendMessage("[c]/[pc]speed [r]<user> <speed>");
        if(!s.hasPermission("speed", "speed.other"))
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Permission", "speed"));
            
    }
    
    float speed(int d) {
        double c = d;
        double p = (c/10);
        int lvl = Integer.parseInt((p+"").split("\\.")[1]);
        if(p == 1) return 1f;
        if(p == -1) return -1f;
        if(p==0)return 0f;
        if(p<0)return -Float.parseFloat("0."+lvl);
        return Float.parseFloat("0."+lvl);
    }
    
}
