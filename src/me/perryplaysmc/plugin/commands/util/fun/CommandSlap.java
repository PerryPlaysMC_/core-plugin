package me.perryplaysmc.plugin.commands.util.fun;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandLabel;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 8/30/19-2023
 * Package: net.miniverse.plugin.commands.util.fun
 * Path: CommandSlap
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandSlap extends Command {
    
    public CommandSlap() {
        super("Slap");
    }
    
    public void run(CommandSource s, CommandLabel commandLabel, Argument[] args) throws CommandException {
        hasPermission("slap");
        User u = getUser(args[0]);
        u.setHealth(u.getHealth()+-integer(args[1]));
        sendMessage("Commands.Slap.slap", u.getName(), integer(args[1]));
    }
    
}
